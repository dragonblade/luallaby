use std::collections::HashMap;
use std::iter::{Copied, Enumerate, Peekable};
use std::rc::Rc;
use std::slice::Iter;

mod types;

use crate::error::{Error, LuaError, Result};
use crate::value::{from_str_radix_wrapping, parse_f64_hex};
pub use types::{Pos, Spanned, Token};

use self::types::StringType;

pub struct Tokenizer<'a> {
    input: Peekable<Enumerate<Copied<Iter<'a, u8>>>>,
    line: usize,
    line_offset: usize,

    last_pos: Pos,

    strings: HashMap<Vec<u8>, Rc<Vec<u8>>>,

    source: Rc<Vec<u8>>,
}

impl<'a> Tokenizer<'a> {
    pub fn read(input: &'a [u8], source: Rc<Vec<u8>>) -> Self {
        Self {
            input: input.iter().copied().enumerate().peekable(),
            line: 0,
            line_offset: 0,

            last_pos: Pos::default(),

            strings: HashMap::new(),

            source,
        }
    }

    pub fn last_pos(&self) -> Pos {
        self.last_pos
    }

    fn line_inc(&mut self, pos: usize) {
        self.line += 1;
        self.line_offset = pos + 1; // Such that the next character gets column 0
    }

    pub fn read_token(&mut self) -> Result<Option<Spanned<Token>>> {
        let (pos, char) = loop {
            self.consume_whitespace();
            let (pos, char) = match self.input.next() {
                Some(res) => res,
                None => {
                    self.last_pos = Pos(self.line as i64 + 1, 1);
                    return Ok(None);
                }
            };
            if char == b'-' && matches!(self.input.peek(), Some((_, b'-'))) {
                self.input.next();
                self.consume_comment();
            } else {
                break (pos, char);
            }
        };
        let pos = Pos(
            self.line as i64 + 1,
            pos.saturating_sub(self.line_offset) as i64 + 1,
        );
        self.last_pos = pos;

        let token = match char {
            b'+' => Token::Add,
            b'-' => Token::Sub,
            b'*' => Token::Mul,
            b'/' => {
                if let Some((_, b'/')) = self.input.peek() {
                    self.input.next();
                    Token::FlDiv
                } else {
                    Token::Div
                }
            }
            b'%' => Token::Mod,
            b'^' => Token::Pow,
            b'#' => Token::Hash,
            b'&' => Token::Amp,
            b'~' => {
                if let Some((_, b'=')) = self.input.peek() {
                    self.input.next();
                    Token::Neq
                } else {
                    Token::Tilde
                }
            }
            b'|' => Token::Bar,
            b'<' => {
                if let Some((_, b'<')) = self.input.peek() {
                    self.input.next();
                    Token::Shl
                } else if let Some((_, b'=')) = self.input.peek() {
                    self.input.next();
                    Token::Leq
                } else {
                    Token::Lt
                }
            }
            b'>' => {
                if let Some((_, b'>')) = self.input.peek() {
                    self.input.next();
                    Token::Shr
                } else if let Some((_, b'=')) = self.input.peek() {
                    self.input.next();
                    Token::Geq
                } else {
                    Token::Gt
                }
            }
            b'=' => {
                if let Some((_, b'=')) = self.input.peek() {
                    self.input.next();
                    Token::Eq
                } else {
                    Token::Is
                }
            }
            b'(' => Token::ParO,
            b')' => Token::ParC,
            b'{' => Token::CurlO,
            b'}' => Token::CurlC,
            b'[' => match self.input.peek() {
                Some((_, b'[')) | Some((_, b'=')) => self.consume_long_string()?,
                _ => Token::SqrO,
            },
            b']' => Token::SqrC,
            b':' => {
                if let Some((_, b':')) = self.input.peek() {
                    self.input.next();
                    Token::Ass
                } else {
                    Token::Colon
                }
            }
            b';' => Token::Semi,
            b',' => Token::Comma,
            b'.' => match self.input.peek().cloned() {
                Some((_, b'.')) => {
                    self.input.next();
                    if let Some((_, b'.')) = self.input.peek() {
                        self.input.next();
                        Token::Dots
                    } else {
                        Token::Conc
                    }
                }
                Some((_, c)) if c.is_ascii_digit() => self.consume_numeric(b'.')?,
                _ => Token::Point,
            },
            c if is_start_name_char(c) => self.consume_name(c),
            c if matches!(c, b'"' | b'\'') => self.consume_string(c)?,
            c if c.is_ascii_digit() => self.consume_numeric(c)?,
            c => {
                return err!(
                    self.source,
                    self.last_pos,
                    LuaError::LexSymbol(char::from(c))
                )
            }
        };

        Ok(Some(Spanned { inner: token, pos }))
    }

    fn consume_whitespace(&mut self) {
        while let Some((_, peek)) = self.input.peek() {
            match *peek {
                b'\n' => {
                    let pos = self.input.next().unwrap().0;
                    self.line_inc(pos);
                    if let Some((_, b'\r')) = self.input.peek() {
                        self.input.next();
                    }
                }
                b'\r' => {
                    let pos = self.input.next().unwrap().0;
                    self.line_inc(pos);
                    if let Some((_, b'\n')) = self.input.peek() {
                        self.input.next();
                    }
                }
                b'\t'..=b'\r' | b' ' => {
                    self.input.next();
                }
                _ => break,
            }
        }
    }

    fn consume_name(&mut self, start: u8) -> Token {
        assert!(is_start_name_char(start));

        let mut name = vec![start];
        while let Some((_, peek)) = self.input.peek().cloned() {
            if is_name_char(peek) {
                self.input.next();
                name.push(peek);
            } else {
                break;
            }
        }

        let name: String = name.into_iter().map(char::from).collect();
        match name.as_str() {
            "and" => Token::And,
            "break" => Token::Break,
            "do" => Token::Do,
            "else" => Token::Else,
            "elseif" => Token::Elseif,
            "end" => Token::End,
            "false" => Token::False,
            "for" => Token::For,
            "function" => Token::Function,
            "goto" => Token::Goto,
            "if" => Token::If,
            "in" => Token::In,
            "local" => Token::Local,
            "nil" => Token::Nil,
            "not" => Token::Not,
            "or" => Token::Or,
            "repeat" => Token::Repeat,
            "return" => Token::Return,
            "then" => Token::Then,
            "true" => Token::True,
            "until" => Token::Until,
            "while" => Token::While,
            _ => Token::Ident(name),
        }
    }

    fn consume_string(&mut self, delim: u8) -> Result<Token> {
        let mut string = vec![];

        loop {
            match self.input.next() {
                Some((_, next)) => match next {
                    c if c == delim => break,
                    b'\n' => return err!(self.source, self.last_pos, LuaError::LexStringNewline),
                    b'\\' => match self.input.next() {
                        Some((pos, next)) => match next {
                            b'a' => string.push(b'\x07'),
                            b'b' => string.push(b'\x08'),
                            b't' => string.push(b'\t'),
                            b'n' => string.push(b'\n'),
                            b'v' => string.push(b'\x0B'),
                            b'f' => string.push(b'\x0C'),
                            b'r' => string.push(b'\x0D'),
                            b'\\' | b'"' | b'\'' => string.push(next),
                            b'\n' => {
                                self.line_inc(pos);
                                if let Some((_, b'\r')) = self.input.peek() {
                                    self.input.next();
                                }
                                string.push(b'\n')
                            }
                            b'\r' => {
                                self.line_inc(pos);
                                if let Some((_, b'\n')) = self.input.peek() {
                                    self.input.next();
                                }
                                string.push(b'\n');
                            }
                            b'z' => self.consume_whitespace(),
                            b'x' => {
                                let d1 = match self.input.next() {
                                    Some((_, d)) => d,
                                    None => {
                                        return err!(
                                            self.source,
                                            self.last_pos,
                                            LuaError::LexHexDigit(format!(
                                                "{}\\x",
                                                String::from_utf8_lossy(&string),
                                            ))
                                        )
                                    }
                                };
                                if !d1.is_ascii_hexdigit() {
                                    return err!(
                                        self.source,
                                        self.last_pos,
                                        LuaError::LexHexDigit(format!(
                                            "{}\\x{}",
                                            String::from_utf8_lossy(&string),
                                            char::from(d1)
                                        ))
                                    );
                                }

                                let d2 = match self.input.next() {
                                    Some((_, d)) => d,
                                    None => {
                                        return err!(
                                            self.source,
                                            self.last_pos,
                                            LuaError::LexHexDigit(format!(
                                                "{}\\x{}",
                                                String::from_utf8_lossy(&string),
                                                char::from(d1)
                                            ))
                                        )
                                    }
                                };
                                if !d2.is_ascii_hexdigit() {
                                    return err!(
                                        self.source,
                                        self.last_pos,
                                        LuaError::LexHexDigit(format!(
                                            "{}\\x{}{}",
                                            String::from_utf8_lossy(&string),
                                            char::from(d1),
                                            char::from(d2)
                                        ))
                                    );
                                }

                                let digits: String =
                                    vec![d1, d2].into_iter().map(char::from).collect();
                                let value = u8::from_str_radix(&digits, 16).unwrap();
                                string.push(value);
                            }
                            c if c.is_ascii_digit() => {
                                let mut digits = vec![c];
                                if let Some((_, peek)) = self.input.peek().cloned() {
                                    if peek.is_ascii_digit() {
                                        self.input.next();
                                        digits.push(peek);
                                    }
                                }
                                if let Some((_, peek)) = self.input.peek().cloned() {
                                    if peek.is_ascii_digit() {
                                        self.input.next();
                                        digits.push(peek);
                                    }
                                }

                                let mut digits: String =
                                    digits.into_iter().map(char::from).collect();
                                let value = digits.parse::<u8>().map_err(|_| {
                                    if let Some((_, b'"')) = self.input.next() {
                                        digits.push('"');
                                    }
                                    Error::from_lua(LuaError::LexDecimalEscape(format!(
                                        "{}\\{}",
                                        String::from_utf8_lossy(&string),
                                        digits
                                    )))
                                })?;
                                string.push(value);
                            }
                            b'u' => {
                                match self.input.next() {
                                    Some((_, b'{')) => {}
                                    c => {
                                        return err!(
                                            self.source,
                                            self.last_pos,
                                            LuaError::LexMissing(
                                                '{',
                                                format!(
                                                    "{}\\u{}",
                                                    String::from_utf8_lossy(&string),
                                                    c.map(|(_, c)| char::from(c))
                                                        .unwrap_or_default()
                                                )
                                            )
                                        )
                                    }
                                }
                                let mut digits = vec![];
                                loop {
                                    match self.input.next() {
                                        Some((_, c)) if c.is_ascii_hexdigit() => digits.push(c),
                                        Some((_, b'}')) => break,
                                        c => {
                                            return err!(
                                                self.source,
                                                self.last_pos,
                                                LuaError::LexMissing(
                                                    '}',
                                                    format!(
                                                        "{}\\u{{{}{}",
                                                        String::from_utf8_lossy(&string),
                                                        String::from_utf8_lossy(&digits),
                                                        c.map(|(_, c)| char::from(c).to_string())
                                                            .unwrap_or_default()
                                                    )
                                                )
                                            )
                                        }
                                    }
                                }
                                if digits.is_empty() {
                                    return err!(
                                        self.source,
                                        self.last_pos,
                                        LuaError::LexHexDigit(format!(
                                            "{}\\u{{}}",
                                            String::from_utf8_lossy(&string)
                                        ))
                                    );
                                }

                                let digits: String = digits.into_iter().map(char::from).collect();
                                let value = u32::from_str_radix(&digits, 16).map_err(|_| {
                                    Error::from_lua(LuaError::LexUtf8TooLarge(format!(
                                        "{}\\u{{{}",
                                        String::from_utf8_lossy(&string),
                                        digits
                                    )))
                                })?;
                                match value {
                                    0x0..=0x7F => string.push(value as u8),
                                    0x8000_0000.. => panic!(),
                                    mut code => {
                                        let mut bytes = Vec::with_capacity(6);
                                        let mut mfb = 0x3F; // Max that fits in first byte
                                        while code > mfb {
                                            bytes.push((0x80 | (code & 0x3F)) as u8);
                                            mfb >>= 1;
                                            code >>= 6;
                                        }
                                        bytes.push(((!mfb << 1) | code) as u8);

                                        string.extend(bytes.iter().rev());
                                    }
                                }
                            }
                            c => {
                                return err!(
                                    self.source,
                                    self.last_pos,
                                    LuaError::LexStringEscape(format!(
                                        "{}\\{}",
                                        String::from_utf8_lossy(&string),
                                        char::from(c)
                                    ))
                                )
                            }
                        },
                        None => return err!(self.source, self.last_pos, LuaError::LexString),
                    },
                    _ => string.push(next),
                },
                None => return err!(self.source, self.last_pos, LuaError::LexString),
            }
        }

        Ok(Token::String(
            self.strings
                .entry(string.clone())
                .or_insert(Rc::new(string))
                .clone(),
            if delim == b'"' {
                StringType::Double
            } else {
                StringType::Single
            },
        ))
    }

    fn consume_long_string(&mut self) -> Result<Token> {
        let mut len = 0;
        loop {
            match self.input.next() {
                Some((_, b'[')) => break,
                Some((_, b'=')) => len += 1,
                _ => {
                    return err!(
                        self.source,
                        self.last_pos,
                        LuaError::LexLongStringDelim(format!("[{}", "=".repeat(len),))
                    )
                }
            }
        }

        let mut string = vec![];
        let mut tainted = false;

        loop {
            match self.input.next() {
                Some((pos, char)) => match char {
                    b']' => {
                        let mut buffer = vec![char];

                        loop {
                            match self.input.peek() {
                                Some((_, peek)) => {
                                    if peek == &b']' || peek != &b'=' {
                                        break;
                                    }
                                    buffer.push(self.input.next().unwrap().1);
                                }
                                None => {
                                    return err!(
                                        self.source,
                                        self.last_pos,
                                        LuaError::LexLongString
                                    )
                                }
                            }
                        }

                        if len + 1 == buffer.len() && matches!(self.input.peek(), Some((_, b']'))) {
                            self.input.next();
                            break;
                        } else {
                            string.extend(buffer);
                        }
                    }
                    b'\n' => {
                        self.line_inc(pos);
                        if let Some((_, b'\r')) = self.input.peek() {
                            self.input.next();
                        }

                        if tainted {
                            string.push(b'\n');
                        }
                    }
                    b'\r' => {
                        self.line_inc(pos);
                        if let Some((_, b'\n')) = self.input.peek() {
                            self.input.next();
                        }

                        if tainted {
                            string.push(b'\n');
                        }
                    }
                    c => string.push(c),
                },
                None => return err!(self.source, self.last_pos, LuaError::UnexpectedEof),
            }
            tainted = true;
        }

        Ok(Token::String(
            self.strings
                .entry(string.clone())
                .or_insert(Rc::new(string))
                .clone(),
            StringType::Long(len),
        ))
    }

    fn consume_numeric(&mut self, digit: u8) -> Result<Token> {
        let mut hex = false;
        let mut int = digit != b'.';
        let mut digits = vec![digit];

        if let Some((_, char)) = self.input.peek() {
            if matches!(char, b'X' | b'x') {
                self.input.next();
                if digits.first() == Some(&b'0') {
                    hex = true;
                    digits.clear();
                } else {
                    panic!();
                }
            }
        }

        while let Some((_, c)) = self.input.peek().cloned() {
            match c {
                c if hex && c.is_ascii_hexdigit() => {
                    self.input.next();
                    digits.push(c);
                }
                c if !hex && c.is_ascii_digit() => {
                    self.input.next();
                    digits.push(c);
                }
                b'.' => {
                    if int {
                        self.input.next();
                        digits.push(c);
                        int = false;
                    } else {
                        panic!()
                    }
                }
                b'E' | b'e' => {
                    if hex {
                        panic!()
                    } else {
                        self.input.next();
                        int = false;
                        digits.push(c);

                        match self.input.peek().cloned() {
                            Some((_, b'-')) => {
                                self.input.next();
                                digits.push(b'-');
                            }
                            Some((_, b'+')) => {
                                self.input.next();
                            }
                            _ => {}
                        }

                        loop {
                            match self.input.peek().cloned() {
                                Some((_, c)) if c.is_ascii_digit() => {
                                    self.input.next();
                                    digits.push(c);
                                }
                                _ => break,
                            }
                        }
                        break;
                    }
                }
                b'P' | b'p' => {
                    if hex {
                        self.input.next();
                        int = false;
                        digits.push(c);

                        match self.input.peek().cloned() {
                            Some((_, b'-')) => {
                                self.input.next();
                                digits.push(b'-');
                            }
                            Some((_, b'+')) => {
                                self.input.next();
                            }
                            _ => {}
                        }

                        loop {
                            match self.input.peek().cloned() {
                                Some((_, c)) if c.is_ascii_digit() => {
                                    self.input.next();
                                    digits.push(c);
                                }
                                _ => break,
                            }
                        }
                        break;
                    } else {
                        return err!(
                            self.source,
                            self.last_pos,
                            LuaError::LexNumber(format!(
                                "{}{}",
                                digits.into_iter().map(char::from).collect::<String>(),
                                char::from(c),
                            ))
                        );
                    }
                }
                _ => break,
            }
        }

        let digits: String = digits.into_iter().map(char::from).collect();
        Ok(if int {
            if hex {
                let value = from_str_radix_wrapping(digits.as_bytes(), 16);
                match value {
                    Some(int) => Token::Integer(int, digits),
                    None => Token::Float(digits.parse().unwrap(), digits),
                }
            } else {
                let value = digits.parse::<u64>();
                match value {
                    Ok(int) => Token::Integer(int as i64, digits),
                    Err(..) => Token::Float(digits.parse().unwrap(), digits),
                }
            }
        } else {
            let value: f64 = if hex {
                parse_f64_hex(format!("0x{}", digits).as_bytes())
                    .ok_or_else(|| Error::from_lua(LuaError::LexNumber(format!("0x{}", digits))))?
            } else {
                digits
                    .parse()
                    .map_err(|_| Error::from_lua(LuaError::LexNumber(digits.clone())))?
            };
            Token::Float(value, digits)
        })
    }

    fn consume_comment(&mut self) {
        let mut long = None;
        match self.input.next() {
            Some((_, b'[')) => {
                let mut len = 0;

                loop {
                    match self.input.next() {
                        Some((_, b'[')) => {
                            long = Some(len);
                            break;
                        }
                        Some((_, b'=')) => len += 1,
                        Some((pos, b'\n')) => {
                            self.line_inc(pos);
                            if let Some((_, b'\r')) = self.input.peek() {
                                self.input.next();
                            }
                            break;
                        }
                        Some((pos, b'\r')) => {
                            self.line_inc(pos);
                            if let Some((_, b'\n')) = self.input.peek() {
                                self.input.next();
                            }
                            break;
                        }
                        _ => break,
                    }
                }
            }
            Some((pos, b'\n')) => {
                self.line_inc(pos);
                if let Some((_, b'\r')) = self.input.peek() {
                    self.input.next();
                }
                return;
            }
            Some((pos, b'\r')) => {
                self.line_inc(pos);
                if let Some((_, b'\n')) = self.input.peek() {
                    self.input.next();
                }
                return;
            }
            _ => {}
        }

        while let Some((pos, char)) = self.input.next() {
            match char {
                b']' if long.is_some() => {
                    let mut len = 0;
                    let mut closed = false;

                    loop {
                        match self.input.next() {
                            Some((_, b']')) => {
                                if long == Some(len) {
                                    closed = true;
                                }
                                len = 0;
                            }
                            Some((_, b'=')) => len += 1,
                            Some((pos, b'\n')) => {
                                self.line_inc(pos);
                                if let Some((_, b'\r')) = self.input.peek() {
                                    self.input.next();
                                }
                                break;
                            }
                            Some((pos, b'\r')) => {
                                self.line_inc(pos);
                                if let Some((_, b'\n')) = self.input.peek() {
                                    self.input.next();
                                }
                                break;
                            }
                            _ => break,
                        }
                    }

                    if closed {
                        break;
                    }
                }
                b'\n' => {
                    self.line_inc(pos);
                    if let Some((_, b'\r')) = self.input.peek() {
                        self.input.next();
                    }
                    if long.is_none() {
                        break;
                    }
                }
                b'\r' => {
                    self.line_inc(pos);
                    if let Some((_, b'\n')) = self.input.peek() {
                        self.input.next();
                    }
                    if long.is_none() {
                        break;
                    }
                }
                _ => {}
            }
        }
    }
}

#[inline]
fn is_start_name_char(chr: u8) -> bool {
    matches!(chr, b'A'..=b'Z' | b'a'..=b'z' | b'_')
}

#[inline]
fn is_name_char(chr: u8) -> bool {
    matches!(chr, b'A'..=b'Z' | b'a'..=b'z' | b'0'..=b'9' | b'_')
}
