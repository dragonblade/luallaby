use crate::ast::{Block, Exp, Parser};
use crate::error::Result;
use crate::lexer::{Pos, Token};

/// ```test
/// stat ::= while exp do block end
/// ```
pub struct While {
    pub cond: Box<Exp>,
    pub block: Box<Block>,
    pub pos_end: Pos,
}

impl<'a> Parser<'a> {
    pub(super) fn parse_while(&mut self) -> Result<While> {
        tok_expect!(self, Token::While);

        let cond = Box::new(self.parse_exp()?);

        tok_expect!(self, Token::Do);
        let block = Box::new(self.parse_block()?);
        let pos_end = tok_expect!(self, Token::End);

        Ok(While {
            cond,
            block,
            pos_end,
        })
    }
}
