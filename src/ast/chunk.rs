use crate::ast::{Block, Parser};
use crate::error::Result;
use crate::lexer::Pos;

/// ```text
/// chunk ::= block
/// ```
pub struct Chunk {
    pub block: Block,
    pub pos_ret: Pos,
}

impl<'a> Parser<'a> {
    pub(super) fn parse_chunk(&mut self) -> Result<Chunk> {
        Ok(Chunk {
            block: self.parse_block()?,
            pos_ret: self.tokens.last_pos(),
        })
    }
}
