use crate::ast::{Block, Exp, Parser};
use crate::error::Result;
use crate::lexer::{Pos, Token};

/// ```text
/// stat ::= repeat block until exp
/// ```
pub struct Repeat {
    pub block: Box<Block>,
    pub cond: Box<Exp>,
    pub pos_until: Pos,
}

impl<'a> Parser<'a> {
    pub(super) fn parse_repeat(&mut self) -> Result<Repeat> {
        tok_expect!(self, Token::Repeat);

        let block = Box::new(self.parse_block()?);

        let pos_until = tok_expect!(self, Token::Until);

        let cond = Box::new(self.parse_exp()?);

        Ok(Repeat {
            block,
            cond,
            pos_until,
        })
    }
}
