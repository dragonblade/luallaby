use std::rc::Rc;

use crate::error::Result;
use crate::vm::{FuncBuiltin, Numeric, Value, VM};
use crate::LuaError;

use super::index_start;

const MAX_MATCH_DEPTH: usize = 200;

struct Matcher<'a> {
    subject: &'a [u8],
    pattern: &'a [u8],
    next_start: usize,
    next_end: usize,
}

struct Match {
    start: usize,
    end: usize,
    captures: Vec<Capture>,
}

enum Capture {
    Pos(usize),
    Sub { start: usize, end: usize },
}

impl<'a> Matcher<'a> {
    fn new(subject: &'a [u8], pattern: &'a [u8]) -> Self {
        Self {
            subject,
            pattern,
            next_start: 0,
            next_end: 0,
        }
    }
}

impl<'a> Iterator for Matcher<'a> {
    type Item = Result<Match>;

    fn next(&mut self) -> Option<Self::Item> {
        // Handle ^ at start of string
        if self.next_start == 0 && self.pattern.first() == Some(&b'^') {
            self.next_start = self.subject.len() + 1;
            return Find::find(0, self.subject, &self.pattern[1..]);
        }

        while self.next_start <= self.subject.len() {
            if let Some(mat) = Find::find(self.next_start, self.subject, self.pattern) {
                match mat {
                    Ok(mat) => {
                        if mat.end >= self.next_end {
                            self.next_start += 1;
                            self.next_end = mat.end + 1;
                            return Some(Ok(mat));
                        }
                    }
                    // Returning Some(Err(e)) here is a bit dangerous,
                    // if the caller does not break the loop when finding
                    // an error it will loop infinitely, returning the
                    // same error since this is not incrementing the
                    // `next_start` counter.
                    // However, incrementing `next_start` would not be a
                    // good idea since then the error will stop being
                    // produced at some point, even though from the
                    // callers point of view nothing should change, which
                    // is undesireable.
                    Err(e) => return Some(Err(e)),
                }
            }
            self.next_start += 1;
        }

        None
    }
}

#[derive(Clone)]
struct Find<'a> {
    subject: &'a [u8],
    pattern: &'a [u8],
    pos_subj: usize,
    pos_pat: usize,
    caps: Vec<FindCap>,
    depth: usize,
}

#[derive(Clone)]
enum FindCap {
    Capture { start: usize, end: usize },
    Partial(usize),
    Pos(usize),
}

macro_rules! some {
    ( $e:expr ) => {
        match $e {
            Some(x) => x,
            None => return Ok(false),
        }
    };
}

impl<'a> Find<'a> {
    fn find(offset: usize, subject: &'a [u8], pattern: &'a [u8]) -> Option<Result<Match>> {
        let mut find = Self {
            subject,
            pattern,
            pos_subj: offset,
            pos_pat: 0,
            caps: Vec::new(),
            depth: 0,
        };
        let end = match find.r#match() {
            Ok(end) => end.then_some(find.pos_subj)?,
            Err(e) => return Some(Err(e)),
        };
        let mut captures = Vec::with_capacity(find.caps.len());
        for cap in find.caps {
            match cap {
                FindCap::Capture { start, end } => captures.push(Capture::Sub { start, end }),
                FindCap::Partial(_) => return None,
                FindCap::Pos(pos) => captures.push(Capture::Pos(pos)),
            }
        }
        Some(Ok(Match {
            start: offset,
            end,
            captures,
        }))
    }

    fn r#match(&mut self) -> Result<bool> {
        self.depth += 1;
        if self.depth > MAX_MATCH_DEPTH {
            return err!(LuaError::StringPatternDepth);
        }

        while let Some(p) = self.pattern.get(self.pos_pat) {
            match p {
                b'$' if self.pos_pat + 1 == self.pattern.len() => {
                    if self.pos_subj != self.subject.len() {
                        return Ok(false);
                    }
                    self.pos_pat += 1;
                }
                b'(' => {
                    if self.pattern.get(self.pos_pat + 1) == Some(&b')') {
                        self.caps.push(FindCap::Pos(self.pos_subj));
                        self.pos_pat += 2;
                    } else {
                        self.caps.push(FindCap::Partial(self.pos_subj));
                        self.pos_pat += 1;
                    }
                }
                b')' => {
                    let mut iter = self.caps.iter_mut().rev();
                    loop {
                        match iter.next() {
                            Some(cap) => {
                                if let FindCap::Partial(start) = cap {
                                    *cap = FindCap::Capture {
                                        start: *start,
                                        end: self.pos_subj,
                                    };
                                    break;
                                }
                            }
                            None => return err!(LuaError::StringPatternNoCapture),
                        }
                    }
                    self.pos_pat += 1;
                }
                b'%' if self.pos_pat + 1 >= self.pattern.len() => {
                    return err!(LuaError::StringPatternEscape);
                }
                b'%' if matches!(self.pattern.get(self.pos_pat + 1), Some(b'0'..=b'9')) => {
                    let cap = self.pattern.get(self.pos_pat + 1).unwrap();
                    if cap == &b'0' {
                        return err!(LuaError::StringPatternInvalidCapture(0));
                    }
                    let cap = match self.caps.get((cap - b'1') as usize) {
                        None | Some(FindCap::Partial(..)) => {
                            return err!(LuaError::StringPatternInvalidCapture(cap - b'0'));
                        }
                        Some(cap) => cap,
                    };
                    match cap {
                        FindCap::Capture { start, end } => {
                            let len = end - start;
                            if self.pos_subj + len > self.subject.len()
                                || self.subject[*start..*end]
                                    != self.subject[self.pos_subj..self.pos_subj + len]
                            {
                                return Ok(false);
                            }
                            self.pos_subj += len;
                        }
                        _ => return Ok(false),
                    }
                    self.pos_pat += 2;
                }
                b'%' if matches!(self.pattern.get(self.pos_pat + 1), Some(b'b')) => {
                    if self.pos_pat + 3 >= self.pattern.len() {
                        return err!(LuaError::StringPatternBalance);
                    }
                    let open = self.pattern.get(self.pos_pat + 2).unwrap();
                    let close = self.pattern.get(self.pos_pat + 3).unwrap();
                    if self.subject.get(self.pos_subj) != Some(open) {
                        return Ok(false);
                    }
                    self.pos_subj += 1;
                    let mut depth = 1;
                    while depth > 0 {
                        match self.subject.get(self.pos_subj) {
                            None => return Ok(false),
                            Some(c) if c == close => depth -= 1,
                            Some(c) if c == open => depth += 1,
                            _ => {}
                        }
                        self.pos_subj += 1;
                    }
                    self.pos_pat += 4;
                }
                b'%' if matches!(self.pattern.get(self.pos_pat + 1), Some(b'f')) => {
                    if self.pattern.get(self.pos_pat + 2) != Some(&b'[') {
                        return err!(LuaError::StringPatternFrontier);
                    } else if self.set(
                        if self.pos_subj > 0 {
                            some!(self.subject.get(self.pos_subj - 1))
                        } else {
                            &b'\0'
                        },
                        self.pos_pat + 3,
                    ) || !self.set(
                        self.subject.get(self.pos_subj).unwrap_or(&b'\0'),
                        self.pos_pat + 3,
                    ) {
                        return Ok(false);
                    }
                    self.pos_pat = match self.modifier(self.pos_pat + 2) {
                        Ok(pos) => pos,
                        Err(e) => return Err(e),
                    };
                }
                _ => {
                    let modifier = match self.modifier(self.pos_pat) {
                        Ok(pos) => pos,
                        Err(e) => return Err(e),
                    };
                    match self.pattern.get(modifier) {
                        Some(c) if matches!(c, b'*' | b'+') => {
                            if matches!(c, b'+') {
                                if !self
                                    .single(some!(self.subject.get(self.pos_subj)), self.pos_pat)
                                {
                                    return Ok(false);
                                }
                                self.pos_subj += 1;
                            }

                            let mut count = 0;
                            // Match as many as possible
                            while self
                                .subject
                                .get(self.pos_subj + count)
                                .map(|s| self.single(s, self.pos_pat))
                                .unwrap_or(false)
                            {
                                count += 1;
                            }

                            // Remove matches until rest passes
                            while count > 0 {
                                let mut find = self.clone();
                                find.pos_subj = self.pos_subj + count;
                                find.pos_pat = modifier + 1;
                                if find.r#match().unwrap_or(false) {
                                    *self = find;
                                    return Ok(true);
                                }
                                count -= 1;
                            }

                            self.pos_subj += count;
                            self.pos_pat = modifier + 1;
                        }
                        Some(b'-') => {
                            loop {
                                let mut find = self.clone();
                                find.pos_pat = modifier + 1;
                                if find.r#match()? {
                                    *self = find;
                                    return Ok(true);
                                } else if self
                                    .subject
                                    .get(self.pos_subj)
                                    .map(|s| self.single(s, self.pos_pat))
                                    .unwrap_or(false)
                                {
                                    self.pos_subj += 1;
                                } else {
                                    break;
                                }
                            }
                            self.pos_pat = modifier + 1;
                        }
                        Some(b'?') => {
                            if let Some(s) = self.subject.get(self.pos_subj) {
                                if self.single(s, self.pos_pat) {
                                    let mut find = self.clone();
                                    find.pos_subj += 1;
                                    find.pos_pat = modifier + 1;
                                    if find.r#match()? {
                                        *self = find;
                                        return Ok(true);
                                    }
                                }
                            }
                            self.pos_pat = modifier + 1;
                        }
                        _ => {
                            if !self.single(some!(self.subject.get(self.pos_subj)), self.pos_pat) {
                                return Ok(false);
                            }
                            self.pos_subj += 1;
                            self.pos_pat = modifier;
                        }
                    }
                }
            }
        }

        if self
            .caps
            .iter()
            .any(|cap| matches!(cap, FindCap::Partial(..)))
        {
            err!(LuaError::StringPatternUnfinishedCapture)
        } else {
            Ok(true)
        }
    }

    fn single(&self, s: &u8, pos_pat: usize) -> bool {
        match self.pattern.get(pos_pat) {
            Some(b'.') => true,
            Some(b'%') => self.class(s, pos_pat + 1),
            Some(b'[') => self.set(s, pos_pat + 1),
            Some(c) => s == c,
            None => false,
        }
    }

    fn class(&self, s: &u8, pos_pat: usize) -> bool {
        match self.pattern.get(pos_pat) {
            Some(c) => {
                (match c.to_ascii_lowercase() {
                    b'a' => s.is_ascii_alphabetic(),
                    b'c' => s.is_ascii_control(),
                    b'd' => s.is_ascii_digit(),
                    b'g' => s.is_ascii_graphic(),
                    b'l' => s.is_ascii_lowercase(),
                    b'p' => s.is_ascii_punctuation(),
                    b's' => s.is_ascii_whitespace(),
                    b'u' => s.is_ascii_uppercase(),
                    b'w' => s.is_ascii_alphanumeric(),
                    b'x' => s.is_ascii_hexdigit(),
                    b'z' => s == &b'\0',
                    _ => return s == c,
                } ^ c.is_ascii_uppercase())
            }
            None => false,
        }
    }

    fn set(&self, s: &u8, mut pos_pat: usize) -> bool {
        let sig = self.pattern.get(pos_pat) != Some(&b'^');
        if !sig {
            pos_pat += 1;
        }

        // ] as first character in set
        if self.pattern.get(pos_pat) == Some(&b']') {
            if s == &b']' {
                return sig;
            }
            pos_pat += 1;
        }

        while let Some(c) = self.pattern.get(pos_pat) {
            match c {
                b']' => break,
                b'%' => {
                    if self.class(s, pos_pat + 1) {
                        return sig;
                    }
                    pos_pat += 2;
                }
                b'-' if self.pattern.get(pos_pat + 1) == Some(&b']') => return s == &b'-',
                f if self.pattern.get(pos_pat + 1) == Some(&b'-')
                    && !matches!(self.pattern.get(pos_pat + 2), None | Some(&b']')) =>
                {
                    let t = self.pattern.get(pos_pat + 2).unwrap();
                    if (f..=t).contains(&s) {
                        return sig;
                    }
                    pos_pat += 3;
                }
                c => {
                    if s == c {
                        return sig;
                    }
                    pos_pat += 1;
                }
            }
        }
        !sig
    }

    fn modifier(&self, pos_pat: usize) -> Result<usize> {
        match self.pattern.get(pos_pat) {
            Some(b'%') => Ok(pos_pat + 2),
            Some(b'[') => {
                let mut pos_pat = pos_pat + 1;
                if self.pattern.get(pos_pat) == Some(&b'^') {
                    pos_pat += 1;
                }
                if self.pattern.get(pos_pat) == Some(&b']') {
                    // Skip ] as first character
                    pos_pat += 1;
                }
                loop {
                    match self.pattern.get(pos_pat) {
                        Some(b']') => break,
                        Some(b'%') => pos_pat += 2,
                        Some(_) => pos_pat += 1,
                        None => return err!(LuaError::StringPatternSet),
                    }
                }
                Ok(pos_pat + 1)
            }
            _ => Ok(pos_pat + 1),
        }
    }
}

pub(super) fn find(vm: &mut VM) -> Result<Value> {
    let s = vm.arg_string_coerce(0)?;
    let pattern = vm.arg_string_coerce(1)?;

    let offset = match vm.arg_or_nil(2) {
        Value::Nil => 0,
        v => index_start(v, s.len())?,
    };
    if offset > s.len() {
        return Ok(Value::Nil);
    }
    let s = &s[offset..];

    Ok(if vm.arg_or_nil(3).is_truthy() {
        if pattern.is_empty() {
            Value::Mult(vec![
                Value::Number(Numeric::from_usize_try_int(offset + 1)),
                Value::Number(Numeric::from_usize_try_int(offset)),
            ])
        } else {
            // No pattern matching
            match s.windows(pattern.len()).position(|sub| sub == pattern) {
                Some(idx) => Value::Mult(vec![
                    Value::Number(Numeric::from_usize_try_int(idx + offset + 1)),
                    Value::Number(Numeric::from_usize_try_int(idx + offset + pattern.len())),
                ]),
                None => Value::Nil,
            }
        }
    } else {
        let mut matcher = Matcher::new(s, &pattern);

        match matcher.next().transpose()? {
            Some(m) => Value::Mult(vec![
                Value::Number(Numeric::from_usize_try_int(m.start + offset + 1)),
                Value::Number(Numeric::from_usize_try_int(m.end + offset)),
            ]),
            None => Value::Nil,
        }
    })
}

pub(super) fn gmatch(vm: &mut VM) -> Result<Value> {
    let s = vm.arg_string_coerce(0)?;
    let len = s.len();
    let pattern = vm.arg_string_coerce(1)?;
    let offset = match vm.arg_or_nil(2) {
        Value::Nil => 0,
        v => index_start(v, len)?,
    };
    let s = if offset > len { &s[0..0] } else { &s[offset..] };

    let tbl = vm.alloc_table();
    {
        let mut tbl = tbl.borrow_mut();
        let matcher = Matcher::new(s, &pattern);
        if offset <= len {
            for (i, m) in matcher.enumerate() {
                let m = m?;
                tbl.set(
                    Value::int(i as i64 + 1),
                    if m.captures.is_empty() {
                        Value::str_bytes(s[m.start..m.end].to_vec())
                    } else {
                        Value::Mult(
                            m.captures
                                .iter()
                                .map(|cap| match cap {
                                    Capture::Pos(pos) => Value::int(*pos as i64 + 1),
                                    Capture::Sub { start, end } => {
                                        Value::str_bytes(s[*start..*end].to_vec())
                                    }
                                })
                                .collect(),
                        )
                    },
                )?;
            }
        }
        tbl.set(Value::str("next"), Value::int(1))?;
    }

    Ok(Value::Mult(vec![Value::Func(vm.alloc_builtin(
        FuncBuiltin {
            module: "string",
            name: "__gmatch",
            func: Rc::new(move |_| {
                let tbl = tbl.clone();
                let next = tbl.borrow().get(&Value::str("next"));
                let res = tbl.borrow().get(&next);
                tbl.borrow_mut().set(
                    Value::str("next"),
                    Value::int(next.to_number()?.to_int()? + 1),
                )?;
                Ok(res)
            }),
        },
    ))]))
}

pub(super) fn gsub(vm: &mut VM) -> Result<Value> {
    let s = vm.arg(0)?;
    let str = s.to_string_coerce()?;
    let pattern = vm.arg_string_coerce(1)?;
    let repl = vm.arg(2)?;
    let n = match vm.arg_or_nil(3) {
        Value::Nil => 0,
        v => v.to_int_coerce()? as usize,
    };

    let mut replaced = Vec::new();
    let mut replaced_until = 0;
    let mut replace_count = 0;
    let mut done = 0;
    let mut matcher = Matcher::new(&str, &pattern);
    while let Some(mat) = matcher.next() {
        let mat = mat?;
        matcher.next_start = mat.end.max(matcher.next_start);
        if replaced_until < mat.start {
            replaced.extend(&str[replaced_until..mat.start]);
        }
        replace_count += 1;

        match &repl {
            Value::String(repl) => {
                let mut repl = repl.iter().copied();
                while let Some(c) = repl.next() {
                    match c {
                        b'%' => match repl.next() {
                            Some(b'%') => replaced.push(b'%'),
                            Some(b'0') => replaced.extend(&str[mat.start..mat.end]),
                            Some(c) if matches!(c, b'1'..=b'9') => {
                                if mat.captures.is_empty() && c == b'1' {
                                    replaced.extend(&str[mat.start..mat.end]);
                                } else if let Some(cap) = mat.captures.get((c - b'1') as usize) {
                                    match cap {
                                        Capture::Pos(pos) => {
                                            replaced.extend((pos + 1).to_string().as_bytes())
                                        }
                                        Capture::Sub { start, end } => {
                                            replaced.extend(&str[*start..*end])
                                        }
                                    }
                                } else {
                                    return err!(LuaError::StringPatternInvalidCapture(c - b'0'));
                                }
                            }
                            _ => return err!(LuaError::StringGSubInvalidEscape),
                        },
                        c => replaced.push(c),
                    }
                }
            }
            Value::Func(func) => {
                let args = if mat.captures.is_empty() {
                    vec![Value::str_bytes(str[mat.start..mat.end].to_vec())]
                } else {
                    mat.captures
                        .iter()
                        .map(|cap| match cap {
                            Capture::Pos(pos) => Value::int(*pos as i64 + 1),
                            Capture::Sub { start, end } => {
                                Value::str_bytes(str[*start..*end].to_vec())
                            }
                        })
                        .collect()
                };
                let res = vm.call_recursive(func.clone(), args)?;
                if res.is_falsy() {
                    // Do not count as replacement
                    replace_count -= 1;
                    replaced.extend(&str[mat.start..mat.end]);
                } else {
                    replaced.extend(res.to_string_coerce()?);
                }
            }
            Value::Table(tbl) => {
                let key = if mat.captures.is_empty() {
                    Value::str_bytes(str[mat.start..mat.end].to_vec())
                } else {
                    let cap = mat.captures.first().unwrap();
                    match cap {
                        Capture::Pos(pos) => Value::int(*pos as i64 + 1),
                        Capture::Sub { start, end } => Value::str_bytes(str[*start..*end].to_vec()),
                    }
                };
                // TODO: refactor when metatables fully implemented
                let res = if let Some(func) = tbl
                    .borrow()
                    .get_meta()
                    .as_ref()
                    .map(|meta| meta.borrow().get(&Value::str("__index")))
                {
                    let func = func.to_func()?;
                    vm.call_recursive(func, vec![Value::Table(tbl.clone()), key])?
                } else {
                    tbl.borrow().get(&key)
                };
                if res.is_falsy() {
                    // Do not count as replacement
                    replace_count -= 1;
                    replaced.extend(&str[mat.start..mat.end]);
                } else {
                    replaced.extend(res.to_string_coerce().map_err(|err| {
                        err.map_lua_error(|_| {
                            LuaError::StringGSubReplacementValue(res.value_type())
                        })
                    })?);
                }
            }
            _ => return err!(LuaError::StringGSubArgument),
        }

        replaced_until = mat.end;
        done += 1;
        if n > 0 && done == n {
            break;
        }
    }

    if replace_count == 0 {
        // Do not create new string when nothing was replaced
        Ok(Value::Mult(vec![s, Value::int(done as i64)]))
    } else {
        replaced.extend(&str[replaced_until..]);

        Ok(Value::Mult(vec![
            Value::str_bytes(replaced),
            Value::int(done as i64),
        ]))
    }
}

pub(super) fn r#match(vm: &mut VM) -> Result<Value> {
    let s = vm.arg(0)?;
    let s = s.to_string()?;
    let pattern = vm.arg(1)?;
    let pattern = pattern.to_string()?;

    let offset = match vm.arg_or_nil(2) {
        Value::Nil => 0,
        v => index_start(v, s.len())?,
    };
    if offset > s.len() {
        return Ok(Value::Nil);
    }
    let s = &s[offset..];

    let mut matcher = Matcher::new(s, pattern);

    Ok(match matcher.next() {
        Some(m) => {
            let m = m?;
            if m.captures.is_empty() {
                Value::str_bytes(s[m.start..m.end].to_vec())
            } else {
                Value::Mult(
                    m.captures
                        .iter()
                        .map(|cap| match cap {
                            Capture::Pos(pos) => Value::int(*pos as i64 + 1),
                            Capture::Sub { start, end } => {
                                Value::str_bytes(s[*start..*end].to_vec())
                            }
                        })
                        .collect(),
                )
            }
        }
        None => Value::Nil,
    })
}
