use std::io::Write;
use std::rc::Rc;

use crate::error::Result;
use crate::util::SliceExt;
use crate::vm::stdlib::basic::tostring;
use crate::vm::{Numeric, Value, VM};
use crate::{Error, LuaError};

#[derive(Default)]
struct FormatOpt {
    conv: u8,                 // %{specifier}
    just_left: bool,          // %-s
    sign_force: bool,         // %+s
    sign_space: bool,         // % s
    format_alt: bool,         // %#s
    pad_zeroes: bool,         // %02s
    min_width: Option<usize>, // %2s
    precision: Option<usize>, // %.2s
}

pub(super) fn format(vm: &mut VM) -> Result<Value> {
    let format = vm.arg(0)?;
    let format = format.to_string()?;
    let mut args = vm.arg_split(1);
    let mut format = format.iter().copied().peekable();
    let mut out = Vec::new();

    while let Some(char) = format.next() {
        if !matches!(char, b'%') {
            out.push(char);
            continue;
        }
        let mut len = 1;

        // Read modifiers
        let mut opt = FormatOpt::default();
        loop {
            match format.peek().copied() {
                Some(c) if matches!(c, b'-' | b'+' | b' ' | b'#' | b'0') => {
                    format.next();
                    len += 1;
                    match c {
                        b'-' => opt.just_left = true,
                        b'+' => opt.sign_force = true,
                        b' ' => opt.sign_space = true,
                        b'#' => opt.format_alt = true,
                        b'0' => opt.pad_zeroes = true,
                        _ => unreachable!(),
                    }
                }
                _ => break,
            }
        }
        opt.min_width = {
            let mut digits = vec![];
            loop {
                match format.peek().copied() {
                    Some(c) if c.is_ascii_digit() => {
                        format.next();
                        len += 1;
                        digits.push(c);
                    }
                    _ => break,
                }
            }
            if digits.is_empty() {
                None
            } else {
                Some(
                    String::from_utf8_lossy(&digits)
                        .parse()
                        .map_err(|_| Error::from_lua(LuaError::StringFormatTooLong))?,
                )
            }
        };
        opt.precision = match format.peek() {
            Some(b'.') => {
                format.next();
                let mut digits = vec![];
                loop {
                    match format.peek().copied() {
                        Some(c) if c.is_ascii_digit() => {
                            format.next();
                            len += 1;
                            digits.push(c);
                        }
                        _ => break,
                    }
                }
                if digits.is_empty() {
                    Some(0)
                } else {
                    Some(
                        String::from_utf8_lossy(&digits)
                            .parse()
                            .map_err(|_| Error::from_lua(LuaError::StringFormatTooLong))?,
                    )
                }
            }
            _ => None,
        };

        // Read conversion specifier
        opt.conv = format
            .next()
            .ok_or_else(|| Error::from_lua(LuaError::StringFormatInvalidSpec))?;
        len += 1;

        // Shortcut, because no need to take an argument
        if opt.conv == b'%' {
            write!(out, "%")?;
            continue;
        }

        if args.is_empty() {
            return err!(LuaError::StringFormatNoValue);
        }
        let arg = args.remove(0);

        if len > 32 {
            return err!(LuaError::StringFormatTooLong);
        } else if opt.min_width.map(|w| w >= 100).unwrap_or(false)
            || opt.precision.map(|p| p >= 100).unwrap_or(false)
        {
            return err!(LuaError::StringFormatInvalidSpec);
        }

        let formatted = format_arg(vm, &opt, arg)?;

        let conv_hex = matches!(opt.conv, b'A' | b'a' | b'p');
        let conv_number = matches!(
            opt.conv,
            b'd' | b'i' | b'o' | b'u' | b'X' | b'x' | b'E' | b'e' | b'f' | b'G' | b'g'
        );
        let min_width = opt.min_width.unwrap_or(0);

        let pad = min_width.saturating_sub(formatted.len());
        if opt.just_left {
            out.extend(formatted);
            out.extend(std::iter::repeat(b' ').take(pad));
        } else if opt.pad_zeroes
            && (conv_hex || (opt.format_alt && matches!(opt.conv, b'X' | b'x')))
        {
            let ind = formatted
                .iter()
                .position(|c| matches!(c, b'x' | b'X'))
                .unwrap();
            out.extend(&formatted[..ind + 1]);
            out.extend(std::iter::repeat(b'0').take(pad));
            out.extend(&formatted[ind + 1..]);
        } else if opt.pad_zeroes && conv_number {
            if formatted
                .first()
                .map(|c| matches!(c, b'+' | b'-' | b' '))
                .unwrap_or(false)
            {
                out.extend(&formatted[..1]);
                out.extend(std::iter::repeat(b'0').take(pad));
                out.extend(&formatted[1..]);
            } else {
                out.extend(std::iter::repeat(b'0').take(pad));
                out.extend(formatted);
            }
        } else {
            out.extend(std::iter::repeat(b' ').take(pad));
            out.extend(formatted);
        }
    }

    Ok(Value::str_bytes(out))
}

fn format_arg(vm: &mut VM, opt: &FormatOpt, arg: Value) -> Result<Vec<u8>> {
    match opt.conv {
        b's' => {
            if opt.pad_zeroes {
                return err!(LuaError::StringFormatInvalidSpec);
            }
            vm.frames.last_mut().unwrap().varargs = vec![arg];
            let mut str = tostring(vm)?.into_string()?;
            if str.last() == Some(&b'\0') {
                return err!(LuaError::StringZeros);
            }
            if let Some(precision) = opt.precision {
                str.truncate(precision);
            }
            Ok(str)
        }
        b'c' => {
            if opt.precision.is_some() || opt.pad_zeroes {
                return err!(LuaError::StringFormatInvalidSpec);
            }
            Ok(vec![arg.to_number_coerce()?.coerce_int()? as u8])
        }
        b'd' | b'i' => {
            if opt.format_alt {
                return err!(LuaError::StringFormatInvalidSpec);
            }
            let n = arg.to_number_coerce()?.coerce_int()?;
            let mut prec = opt.precision.unwrap_or(1);
            let prefix = if n < 0 {
                prec += 1;
                ""
            } else if opt.sign_force {
                "+"
            } else if opt.sign_space {
                " "
            } else {
                ""
            };
            let digits = format!("{:01$}", n, prec);
            Ok(if prec == 0 && n == 0 {
                Vec::new() // Don't output anything
            } else {
                format!("{}{}", prefix, digits).into_bytes()
            })
        }
        b'o' => {
            let n = arg.to_number_coerce()?.coerce_int()? as u64;
            let prec = opt.precision.unwrap_or(1);
            Ok(if prec == 0 && n == 0 {
                if opt.format_alt {
                    vec![b'0']
                } else {
                    Vec::new()
                }
            } else if opt.format_alt && n > 0 {
                format!("0{:01$o}", n, prec).into_bytes()
            } else {
                format!("{:01$o}", n, prec).into_bytes()
            })
        }
        b'u' | b'X' | b'x' => {
            let n = arg.to_number_coerce()?.coerce_int()? as u64;
            let prec = opt.precision.unwrap_or(1);
            Ok(if prec == 0 && n == 0 {
                Vec::new()
            } else {
                match opt.conv {
                    b'u' => format!("{:01$}", n, prec).into_bytes(),
                    b'X' => {
                        if opt.format_alt {
                            format!("0X{:01$X}", n, prec.saturating_sub(2)).into_bytes()
                        } else {
                            format!("{:01$X}", n, prec).into_bytes()
                        }
                    }
                    b'x' => {
                        if opt.format_alt {
                            format!("0x{:01$x}", n, prec.saturating_sub(2)).into_bytes()
                        } else {
                            format!("{:01$x}", n, prec).into_bytes()
                        }
                    }
                    _ => unreachable!(),
                }
            })
        }
        b'A' | b'a' => {
            let n = arg.to_number_coerce()?.to_float();
            let cap = matches!(opt.conv, b'A');
            let mut out = Vec::new();
            // Masks
            const F64_MASK_SIGN: u64 = 0x8000_0000_0000_0000;
            const F64_MASK_EXP: u64 = 0x7ff0_0000_0000_0000;
            const F64_MASK_FRACT: u64 = 0x000f_ffff_ffff_ffff;
            // Parts
            let bits = n.to_bits();
            let neg = bits & F64_MASK_SIGN != 0;
            let exp = (bits & F64_MASK_EXP) >> 52;
            let fract = bits & F64_MASK_FRACT;
            // Write sign
            if neg {
                write!(out, "-")?;
            } else if opt.sign_force {
                write!(out, "+")?;
            } else if opt.sign_space {
                write!(out, " ")?;
            }
            // Write digits and exponent
            if exp == 0x7ff {
                if fract == 0 {
                    write!(out, "{}", if cap { "INF" } else { "inf" })?;
                } else {
                    write!(out, "{}", if cap { "NAN" } else { "nan" })?;
                }
            } else {
                write!(out, "0{}", if cap { 'X' } else { 'x' })?;
                let mut fract_str = if cap {
                    format!("{:X}", fract)
                } else {
                    format!("{:x}", fract)
                };
                let fract_str = if let Some(prec) = opt.precision {
                    if fract_str.len() >= prec {
                        &fract_str[..prec]
                    } else {
                        fract_str.push_str(&"0".repeat(prec - fract_str.len()));
                        fract_str.as_str()
                    }
                } else {
                    fract_str.trim_end_matches('0')
                };
                let exp = if exp == 0x0 {
                    if fract == 0 {
                        write!(out, "0")?;
                        0i64
                    } else {
                        write!(out, "0.{}", fract_str)?;
                        -1022
                    }
                } else {
                    if fract == 0 && !opt.format_alt {
                        write!(out, "1")?;
                    } else {
                        write!(out, "1.{}", fract_str)?;
                    }
                    exp as i64 - 1023
                };
                write!(out, "{}", if cap { 'P' } else { 'p' })?;
                write!(out, "{}", exp)?;
            }
            Ok(out)
        }
        b'E' | b'e' | b'f' => {
            let n = arg.to_number_coerce()?.to_float();
            let prec = opt.precision.unwrap_or(6);
            let prefix = if n.is_sign_negative() {
                "-"
            } else if opt.sign_force {
                "+"
            } else if opt.sign_space {
                " "
            } else {
                ""
            };
            let digits = match opt.conv {
                b'E' => format!("{:.1$E}", n.abs(), prec),
                b'e' => format!("{:.1$e}", n.abs(), prec),
                b'f' => format!("{:.1$}", n.abs(), prec),
                _ => unreachable!(),
            };
            let digits = match opt.conv {
                b'E' | b'e' => {
                    let pos = digits.find(char::from(opt.conv)).unwrap();
                    format!(
                        "{}{}{:+03}",
                        &digits[..pos],
                        char::from(opt.conv),
                        &digits[pos + 1..].parse::<i64>().unwrap()
                    )
                }
                _ => digits,
            };
            let digits = if n.abs().fract() == 0.0 && opt.format_alt {
                match opt.conv {
                    b'E' | b'e' => {
                        let pos = digits.find(char::from(opt.conv)).unwrap();
                        format!("{}.{}", &digits[..pos], &digits[pos..])
                    }
                    b'f' => {
                        format!("{}.", digits)
                    }
                    _ => unreachable!(),
                }
            } else {
                digits
            };
            Ok(format!("{}{}", prefix, digits).into_bytes())
        }
        b'G' | b'g' => {
            let n = arg.to_number_coerce()?.to_float();
            let prec = opt.precision.unwrap_or(6).max(1);
            let e = n.abs().log10().floor();
            Ok(if prec as f64 > e && e >= -4. {
                let prec = prec - 1 - e as usize;
                let digits = format_arg(
                    vm,
                    &FormatOpt {
                        conv: b'f',
                        sign_force: opt.sign_force,
                        sign_space: opt.sign_space,
                        precision: Some(prec),
                        ..Default::default()
                    },
                    Value::float(n),
                )?;
                if opt.format_alt {
                    digits
                } else {
                    digits
                        .trim_end_matches(b'0')
                        .trim_end_matches(b'.')
                        .to_vec()
                }
            } else {
                let prec = prec - 1;
                let format = if matches!(opt.conv, b'G') { b'E' } else { b'e' };
                let digits = format_arg(
                    vm,
                    &FormatOpt {
                        conv: format,
                        sign_force: opt.sign_force,
                        sign_space: opt.sign_space,
                        precision: Some(prec),
                        ..Default::default()
                    },
                    Value::float(n),
                )?;
                if opt.format_alt {
                    digits
                } else {
                    let mut digits = digits;
                    let exp = digits.split_off(digits.iter().position(|c| c == &format).unwrap());
                    let mut out = digits
                        .trim_end_matches(b'0')
                        .trim_end_matches(b'.')
                        .to_vec();
                    out.extend(exp);
                    out
                }
            })
        }
        b'p' => {
            if opt.precision.is_some() {
                return err!(LuaError::StringFormatInvalidSpec);
            }
            let digits = match arg {
                Value::String(s) => {
                    // This is a bit of a hack, Lua does string interning
                    // for short strings (<=40 bytes), since I don't want
                    // to do this, keep a map of strings that is only used
                    // here. This is the only place where strings are
                    // converted to pointers. Comparison is always done on
                    // complete string contents.
                    let ptr = Rc::as_ptr(&s);
                    let ptr = if s.len() <= 40 {
                        *vm.strings.entry(s.to_vec()).or_insert(ptr)
                    } else {
                        ptr
                    };
                    format!("{:p}", ptr)
                }
                Value::Func(f) => format!("{:?}", Rc::as_ptr(&f)),
                Value::Table(r) => format!("{:?}", Rc::as_ptr(&r)),
                Value::Thread(t) => format!("{:?}", Rc::as_ptr(&t)),
                Value::UserData(d) => format!("{:p}", d.as_ptr()),
                _ => "(null)".to_string(),
            };
            let prefix = if digits.starts_with('(') {
                ""
            } else if opt.sign_force {
                "+"
            } else if opt.sign_space {
                " "
            } else {
                ""
            };
            Ok(format!("{}{}", prefix, digits).into_bytes())
        }
        b'q' => {
            if opt.just_left
                || opt.sign_force
                || opt.sign_space
                || opt.format_alt
                || opt.pad_zeroes
                || opt.min_width.is_some()
                || opt.precision.is_some()
            {
                return err!(LuaError::StringFormatQ);
            }
            match arg.into_single() {
                Value::Nil => Ok(b"nil".to_vec()),
                Value::Bool(b) => Ok(format!("{}", b).into_bytes()),
                Value::Number(n) => match n {
                    Numeric::Integer(i) => Ok(format!("{}", i).into_bytes()),
                    Numeric::Float(f) => {
                        if f.is_infinite() {
                            if f.is_sign_negative() {
                                Ok("-1e9999".as_bytes().to_owned())
                            } else {
                                Ok("1e9999".as_bytes().to_owned())
                            }
                        } else if f.is_nan() {
                            Ok("(0/0)".as_bytes().to_owned())
                        } else {
                            format_arg(
                                vm,
                                &FormatOpt {
                                    conv: b'a',
                                    ..Default::default()
                                },
                                Value::float(f),
                            )
                        }
                    }
                },
                Value::String(s) => {
                    let mut out = vec![b'"'];
                    let mut str = s.iter().copied().peekable();
                    while let Some(c) = str.next() {
                        out.extend(match c {
                            b'"' => vec![b'\\', b'"'],
                            b'\\' => vec![b'\\', b'\\'],
                            b'\n' => vec![b'\\', b'\n'],
                            c => {
                                if c.is_ascii_control() {
                                    if str.peek().map(|c| c.is_ascii_digit()).unwrap_or(false) {
                                        format!("\\{:03}", c).into_bytes()
                                    } else {
                                        format!("\\{}", c).into_bytes()
                                    }
                                } else {
                                    vec![c]
                                }
                            }
                        });
                    }
                    out.push(b'"');
                    Ok(out)
                }
                _ => err!(LuaError::StringFormatNoLiteral),
            }
        }
        _ => err!(LuaError::StringFormatInvalidSpec),
    }
}
