use std::collections::{HashMap, HashSet, LinkedList, VecDeque};
use std::fmt;
use std::hash::Hash;
use std::io::{stderr, Write};
use std::{cell::RefCell, rc::Rc};

use crate::vm::Local;

use super::{Error, FuncBuiltin, FuncClosure, FuncDef, LuaError, Result, Table, Thread, Value, VM};

#[derive(Clone, Copy, Debug)]
pub enum GcMode {
    Generational,
    Incremental,
}

impl fmt::Display for GcMode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            GcMode::Generational => f.write_str("generational"),
            GcMode::Incremental => f.write_str("incremental"),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum GcParam {
    Pause,
    StepMul,
}

impl TryFrom<&str> for GcParam {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self> {
        match value {
            "pause" => Ok(Self::Pause),
            "stepmul" => Ok(Self::StepMul),
            _ => err!(LuaError::CollectGarbageOption(value.to_string())),
        }
    }
}

pub struct Heap {
    heap: VecDeque<GcObject>,
    marked: Vec<GcObject>, // Vec to keep track of marking order

    to_collect: HashMap<GcObject, GcData>,
    to_finalize: Vec<(Rc<FuncDef>, Value)>,
    closing: bool,
    stopped: bool,

    // Counters
    prev_size: usize,
    counter: usize,
    offset: usize,

    // GC params
    gc_mode: GcMode,
    gc_pause: f64,
    gc_stepmul: f64,
}

impl Default for Heap {
    fn default() -> Self {
        Self {
            heap: VecDeque::default(),
            marked: Vec::new(),

            to_collect: HashMap::new(),
            to_finalize: Vec::new(),
            closing: false,
            stopped: false,

            prev_size: 10,
            counter: 0,
            offset: 0,

            gc_mode: GcMode::Generational,
            gc_pause: 2.,
            gc_stepmul: 1.,
        }
    }
}

#[derive(Clone)]
enum GcObject {
    Func(Rc<FuncDef>),
    Table(Rc<RefCell<Table>>),
    Thread(Rc<RefCell<Thread>>),
}

impl Hash for GcObject {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            GcObject::Func(func) => {
                state.write_u8(1);
                Rc::as_ptr(func).hash(state)
            }
            GcObject::Table(tbl) => {
                state.write_u8(2);
                Rc::as_ptr(tbl).hash(state)
            }
            GcObject::Thread(thread) => {
                state.write_u8(3);
                Rc::as_ptr(thread).hash(state)
            }
        }
    }
}

impl PartialEq for GcObject {
    fn eq(&self, other: &Self) -> bool {
        std::ptr::addr_eq(self.as_ptr(), other.as_ptr())
    }
}

impl Eq for GcObject {}

impl GcObject {
    fn from(value: &Value) -> Option<Self> {
        match value {
            Value::Func(func) => Some(GcObject::Func(func.clone())),
            Value::Table(tbl) => Some(GcObject::Table(tbl.clone())),
            Value::Thread(thread) => Some(GcObject::Thread(thread.clone())),
            _ => None,
        }
    }

    fn ref_count(&self) -> usize {
        match self {
            GcObject::Func(func) => Rc::strong_count(func),
            GcObject::Table(tbl) => Rc::strong_count(tbl),
            GcObject::Thread(thread) => Rc::strong_count(thread),
        }
    }

    fn as_ptr(&self) -> *const u8 {
        match self {
            GcObject::Func(rc) => Rc::as_ptr(rc) as *const _,
            GcObject::Table(rc) => Rc::as_ptr(rc) as *const _,
            GcObject::Thread(rc) => Rc::as_ptr(rc) as *const _,
        }
    }
}

#[derive(Debug, Default)]
struct GcData {
    count: usize,
    weak_keys: bool,
    weak_vals: bool,
    refs_keys: Vec<(Rc<RefCell<Table>>, Value)>,
    refs_vals: Vec<(Rc<RefCell<Table>>, Value)>,
}

impl Heap {
    fn alloc_func(&mut self, func: FuncDef) -> Rc<FuncDef> {
        let func = Rc::new(func);
        self.heap.push_back(GcObject::Func(func.clone()));
        self.counter += 1;
        func
    }

    fn alloc_string(&mut self) {
        self.counter += 1;
    }

    fn alloc_table(&mut self) -> Rc<RefCell<Table>> {
        let table = Rc::new(RefCell::new(Table::default()));
        self.heap.push_back(GcObject::Table(table.clone()));
        self.counter += 1;
        table
    }

    fn alloc_thread(&mut self, thread: Thread) -> Rc<RefCell<Thread>> {
        let thread = Rc::new(RefCell::new(thread));
        self.heap.push_back(GcObject::Thread(thread.clone()));
        self.counter += 1;
        thread
    }

    fn should_collect(&self) -> bool {
        !self.stopped && self.counter as f64 > self.gc_pause * self.prev_size as f64
    }

    fn consider(&self, max: usize) -> Box<dyn Iterator<Item = &GcObject> + '_> {
        match self.gc_mode {
            GcMode::Incremental => Box::new(self.heap.iter().skip(self.offset).take(max))
                as Box<dyn Iterator<Item = &GcObject>>,
            GcMode::Generational => {
                Box::new(self.heap.iter().rev().take(max)) as Box<dyn Iterator<Item = &GcObject>>
            }
        }
    }

    fn traverse_heap(&mut self, max: usize) {
        // Take ref count before doing anything else
        let mut to_collect = HashMap::new();
        for obj in self.consider(max) {
            let mut count = obj.ref_count();
            // Subtract one for ref in the heap
            count -= 1;
            if self.marked.contains(obj) {
                // Subtract one for ref in the marked set
                count -= 1;
            }
            to_collect.insert(
                obj.clone(),
                GcData {
                    count,
                    ..Default::default()
                },
            );
        }

        macro_rules! track_entry {
            ( $tbl:expr, $key:expr, $val:expr, $weak:expr, $refs:ident ) => {
                if let Some(entry) = GcObject::from(&$val) {
                    to_collect.entry(entry).and_modify(|d| {
                        d.count -= 1;
                        if $weak {
                            d.$refs.push(($tbl.clone(), $key.clone()))
                        }
                    });
                }
            };
        }

        macro_rules! track_value {
            ( $val:expr ) => {
                if let Some(entry) = GcObject::from(&$val) {
                    if let Some(data) = to_collect.get_mut(&entry) {
                        data.count -= 1;
                    }
                }
            };
        }

        let mut ups_visited = HashSet::new();

        for obj in self.heap.iter() {
            let is_considered = to_collect.contains_key(obj);
            match obj {
                GcObject::Table(tbl) => {
                    let mode = if let Some(meta) = tbl.borrow().get_meta() {
                        if is_considered {
                            // Subtract count from metatable
                            if let Some(data) = to_collect.get_mut(&GcObject::Table(meta.clone())) {
                                data.count -= 1;
                            }
                        }
                        meta.borrow()
                            .get(&Value::str("__mode"))
                            .into_string()
                            .unwrap_or_default()
                    } else {
                        Vec::new()
                    };
                    let weak_keys = mode.contains(&b'k');
                    let weak_vals = mode.contains(&b'v');
                    if let Some(data) = to_collect.get_mut(obj) {
                        data.weak_keys = weak_keys;
                        data.weak_vals = weak_vals;
                    }

                    // Reduce count for references from other tracked objects
                    let mut next_key = Value::Nil;
                    loop {
                        if is_considered {
                            track_value!(next_key);
                        }
                        next_key = tbl.borrow().next_key(next_key).unwrap();
                        if is_considered {
                            track_value!(next_key);
                        }

                        if is_considered || weak_keys {
                            track_entry!(tbl, next_key, next_key, weak_keys, refs_keys);
                        }
                        let value = tbl.borrow().get(&next_key);
                        if is_considered || weak_vals {
                            track_entry!(tbl, next_key, value, weak_vals, refs_vals);
                        }

                        if next_key == Value::Nil {
                            break;
                        }
                    }
                }
                GcObject::Func(func) if is_considered => {
                    if let FuncDef::Defined(func) = &**func {
                        for (up, _) in func.ups.iter() {
                            let up = up.borrow();
                            if ups_visited.insert(Rc::as_ptr(&*up)) {
                                track_value!(up.borrow());
                            }
                        }
                    }
                }
                GcObject::Thread(thread) if is_considered => {
                    let thread = thread.borrow();

                    if let Some(func) = &thread.func {
                        if let Some(data) = to_collect.get_mut(&GcObject::Func(func.clone())) {
                            data.count -= 1;
                        }
                    }
                    if let Some(hook) = &thread.hook {
                        if let Some(data) = to_collect.get_mut(&GcObject::Func(hook.clone())) {
                            data.count -= 1;
                        }
                    }

                    for frame in thread.frames.iter().chain(thread.error_close.iter()) {
                        if let Some(data) =
                            to_collect.get_mut(&GcObject::Func(frame.func_def.clone()))
                        {
                            data.count -= 1;
                        }

                        for reg in frame.regs.iter() {
                            track_value!(reg);
                        }

                        for local in frame.locals.iter() {
                            match local {
                                Local::Stack { val, .. } => track_value!(val),
                                Local::Heap { var, .. } => {
                                    if ups_visited.insert(Rc::as_ptr(var)) {
                                        track_value!(var.borrow());
                                    }
                                }
                                Local::Temp => {}
                            }
                        }

                        for (up, _) in frame.ups.iter() {
                            let up = up.borrow();
                            if ups_visited.insert(Rc::as_ptr(&*up)) {
                                track_value!(up.borrow());
                            }
                        }

                        for arg in frame.varargs.iter() {
                            track_value!(arg);
                        }
                    }
                }
                _ => {}
            }
        }

        self.to_collect = to_collect;
    }

    fn traverse_reach(&mut self) {
        let mut reachable = self
            .to_collect
            .iter()
            .filter_map(|(obj, data)| (data.count > 0).then_some(obj))
            .cloned()
            .collect::<LinkedList<_>>();
        let mut ephemeron = VecDeque::new();
        let mut changed = true; // If an ephemeron table has found previously unreachable object
        while changed {
            changed = false;

            while let Some(obj) = reachable.pop_front() {
                // Remove object from data, to mark it as reachable,
                // if object is not in data map anymore, it has been visited before
                if let Some(stat) = self.to_collect.remove(&obj) {
                    // If ephemeron, only consider when done with other reachable objects
                    if stat.weak_keys && !stat.weak_vals && !reachable.is_empty() {
                        ephemeron.push_back(obj);
                        continue;
                    }

                    // Traverse through references, keep them alive
                    match obj {
                        GcObject::Table(tbl) => {
                            if let Some(meta) = tbl.borrow().get_meta() {
                                reachable.push_back(GcObject::Table(meta.clone()));
                            }

                            let mut next_key = Value::Nil;
                            loop {
                                next_key = tbl.borrow().next_key(next_key).unwrap();

                                // Only mark strong references as reachable
                                if !stat.weak_keys {
                                    if let Some(reach) = GcObject::from(&next_key) {
                                        reachable.push_back(reach);
                                    }
                                }
                                if !stat.weak_vals {
                                    let value = tbl.borrow().get(&next_key);
                                    if let Some(reach) = GcObject::from(&value) {
                                        reachable.push_back(reach);
                                    }
                                }

                                if next_key == Value::Nil {
                                    break;
                                }
                            }
                        }
                        GcObject::Func(func) => {
                            if let FuncDef::Defined(func) = &*func {
                                for (up, _) in func.ups.iter() {
                                    if let Some(reach) = GcObject::from(&up.borrow().borrow()) {
                                        reachable.push_back(reach);
                                    }
                                }
                            }
                        }
                        GcObject::Thread(thread) => {
                            let thread = thread.borrow();

                            if let Some(func) = &thread.func {
                                reachable.push_back(GcObject::Func(func.clone()));
                            }
                            if let Some(hook) = &thread.hook {
                                reachable.push_back(GcObject::Func(hook.clone()));
                            }

                            for frame in thread.frames.iter().chain(thread.error_close.iter()) {
                                reachable.push_back(GcObject::Func(frame.func_def.clone()));

                                for reg in frame.regs.iter() {
                                    if let Some(reach) = GcObject::from(reg) {
                                        reachable.push_back(reach);
                                    }
                                }

                                for local in frame.locals.iter() {
                                    match local {
                                        Local::Stack { val, .. } => {
                                            if let Some(reach) = GcObject::from(val) {
                                                reachable.push_back(reach);
                                            }
                                        }
                                        Local::Heap { var, .. } => {
                                            if let Some(reach) = GcObject::from(&var.borrow()) {
                                                reachable.push_back(reach);
                                            }
                                        }
                                        Local::Temp => {}
                                    }
                                }

                                for (up, _) in frame.ups.iter() {
                                    if let Some(reach) = GcObject::from(&up.borrow().borrow()) {
                                        reachable.push_back(reach);
                                    }
                                }

                                for arg in frame.varargs.iter() {
                                    if let Some(reach) = GcObject::from(arg) {
                                        reachable.push_back(reach);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for eph in ephemeron.iter() {
                match eph {
                    GcObject::Table(tbl) => {
                        let mut next_key = Value::Nil;
                        loop {
                            next_key = tbl.borrow().next_key(next_key).unwrap();

                            // Value is reachable only if key is reachable
                            if GcObject::from(&next_key)
                                .map(|obj| !self.to_collect.contains_key(&obj))
                                .unwrap_or(true)
                            {
                                let value = tbl.borrow().get(&next_key);
                                if let Some(reach) = GcObject::from(&value) {
                                    // Only consider if newly reachable
                                    if self.to_collect.contains_key(&reach) {
                                        reachable.push_back(reach);
                                        changed = true;
                                    }
                                }
                            }

                            if next_key == Value::Nil {
                                break;
                            }
                        }
                    }
                    GcObject::Func(func) => {
                        if let FuncDef::Defined(func) = &**func {
                            for (up, _) in func.ups.iter() {
                                if let Some(reach) = GcObject::from(&up.borrow().borrow()) {
                                    // Only consider if newly reachable
                                    if self.to_collect.contains_key(&reach) {
                                        reachable.push_back(reach);
                                        changed = true;
                                    }
                                }
                            }
                        }
                    }
                    GcObject::Thread(thread) => {
                        let thread = thread.borrow();

                        if let Some(func) = &thread.func {
                            let reach = GcObject::Func(func.clone());
                            // Only consider if newly reachable
                            if self.to_collect.contains_key(&reach) {
                                reachable.push_back(reach);
                                changed = true;
                            }
                        }
                        if let Some(hook) = &thread.hook {
                            let reach = GcObject::Func(hook.clone());
                            // Only consider if newly reachable
                            if self.to_collect.contains_key(&reach) {
                                reachable.push_back(reach);
                                changed = true;
                            }
                        }

                        for frame in thread.frames.iter().chain(thread.error_close.iter()) {
                            let reach = GcObject::Func(frame.func_def.clone());
                            // Only consider if newly reachable
                            if self.to_collect.contains_key(&reach) {
                                reachable.push_back(reach);
                                changed = true;
                            }

                            for reg in frame.regs.iter() {
                                if let Some(reach) = GcObject::from(reg) {
                                    // Only consider if newly reachable
                                    if self.to_collect.contains_key(&reach) {
                                        reachable.push_back(reach);
                                        changed = true;
                                    }
                                }
                            }

                            for local in frame.locals.iter() {
                                match local {
                                    Local::Stack { val, .. } => {
                                        if let Some(reach) = GcObject::from(val) {
                                            // Only consider if newly reachable
                                            if self.to_collect.contains_key(&reach) {
                                                reachable.push_back(reach);
                                                changed = true;
                                            }
                                        }
                                    }
                                    Local::Heap { var, .. } => {
                                        if let Some(reach) = GcObject::from(&var.borrow()) {
                                            // Only consider if newly reachable
                                            if self.to_collect.contains_key(&reach) {
                                                reachable.push_back(reach);
                                                changed = true;
                                            }
                                        }
                                    }
                                    Local::Temp => {}
                                }
                            }

                            for (up, _) in frame.ups.iter() {
                                if let Some(reach) = GcObject::from(&up.borrow().borrow()) {
                                    // Only consider if newly reachable
                                    if self.to_collect.contains_key(&reach) {
                                        reachable.push_back(reach);
                                        changed = true;
                                    }
                                }
                            }

                            for arg in frame.varargs.iter() {
                                if let Some(reach) = GcObject::from(arg) {
                                    // Only consider if newly reachable
                                    if self.to_collect.contains_key(&reach) {
                                        reachable.push_back(reach);
                                        changed = true;
                                    }
                                }
                            }
                        }
                    }
                }

                if changed {
                    break;
                }
            }
        }
    }

    fn finalize_prep(&mut self) {
        // Unset weak values before collecting finalizers
        for (_, data) in self.to_collect.iter_mut() {
            for (tbl, key) in data.refs_vals.split_off(0).into_iter() {
                tbl.borrow_mut().set(key, Value::Nil).unwrap();
            }
        }

        // First go through marked objects
        self.marked.retain(|obj| {
            if self.to_collect.contains_key(obj) {
                match obj {
                    GcObject::Table(tbl) => match tbl.borrow().get_meta() {
                        Some(meta) => match meta.borrow().get(&Value::str("__gc")) {
                            Value::Func(gc) => {
                                self.to_finalize
                                    .push((gc.clone(), Value::Table(tbl.clone())));
                                // Has finalizer, keep alive
                                self.to_collect.remove(obj);
                                false
                            }
                            _ => true,
                        },
                        None => true,
                    },
                    GcObject::Func(..) | GcObject::Thread(..) => false, // Never finalized
                }
            } else {
                true
            }
        });

        // Cleanup objects
        for (_, data) in self.to_collect.iter_mut() {
            for (tbl, key) in data.refs_keys.split_off(0).into_iter() {
                // Unset weak keys
                tbl.borrow_mut().set(key, Value::Nil).unwrap();
            }
        }
    }

    fn collect(&mut self) {
        self.heap.retain(|obj| {
            if self.to_collect.remove(obj).is_some() {
                if let GcObject::Table(tbl) = obj {
                    tbl.borrow_mut().clear()
                }
                false
            } else {
                true
            }
        });

        self.prev_size = self.heap.len();
        self.counter = self.prev_size;
    }
}

impl VM {
    pub(super) fn gc_count(&self) -> usize {
        self.heap.heap.len()
    }

    pub(super) fn gc_stopped(&mut self, stopped: Option<bool>) -> bool {
        if let Some(stopped) = stopped {
            self.heap.stopped = stopped;
        }
        self.heap.stopped
    }

    pub(super) fn gc_mode(&mut self, mode: GcMode) -> GcMode {
        std::mem::replace(&mut self.heap.gc_mode, mode)
    }

    pub(super) fn gc_param(&mut self, param: &str, val: f64) -> Result<f64> {
        Ok(match GcParam::try_from(param)? {
            GcParam::Pause => {
                let old = self.heap.gc_pause * 100.;
                self.heap.gc_pause = val / 100.;
                old
            }
            GcParam::StepMul => {
                let old = self.heap.gc_stepmul * 100.;
                self.heap.gc_stepmul = val / 100.;
                old
            }
        })
    }

    pub fn alloc_builtin(&mut self, builtin: FuncBuiltin) -> Rc<FuncDef> {
        let func = self.heap.alloc_func(FuncDef::Builtin(builtin));
        if self.heap.should_collect() {
            self.collect_garbage();
        }
        func
    }

    pub fn alloc_closure(&mut self, closure: FuncClosure) -> Rc<FuncDef> {
        let func = self.heap.alloc_func(FuncDef::Defined(closure));
        if self.heap.should_collect() {
            self.collect_garbage();
        }
        func
    }

    pub fn alloc_string<S: Into<Vec<u8>>>(&mut self, str: S) -> Rc<Vec<u8>> {
        self.heap.alloc_string();
        if self.heap.should_collect() {
            self.collect_garbage();
        }
        Rc::new(str.into())
    }

    pub fn alloc_table(&mut self) -> Rc<RefCell<Table>> {
        let table = self.heap.alloc_table();
        if self.heap.should_collect() {
            self.collect_garbage();
        }
        table
    }

    pub fn alloc_thread(&mut self, thread: Thread) -> Rc<RefCell<Thread>> {
        let thread = self.heap.alloc_thread(thread);
        if self.heap.should_collect() {
            self.collect_garbage();
        }
        thread
    }

    pub fn mark_table(&mut self, table: Rc<RefCell<Table>>) {
        if !self.heap.closing {
            self.heap.marked.push(GcObject::Table(table));
        }
    }

    fn do_collect(&mut self, max: usize) {
        self.heap.traverse_heap(max);
        self.heap.traverse_reach();
        self.heap.finalize_prep();
        self.run_finalizers();
        self.heap.collect();
    }

    pub fn collect_garbage(&mut self) {
        self.heap.offset = 0;
        self.do_collect(usize::MAX);
    }

    pub fn collect_step(&mut self, step: i64) -> bool {
        let max = 2usize.saturating_pow(step.max(0) as u32);
        if self.heap.offset > self.heap.heap.len() {
            self.heap.offset = 0;
        }

        self.do_collect(max);

        self.heap.offset = self.heap.offset.saturating_add(max);
        self.heap.offset >= self.heap.heap.len()
    }

    fn run_finalizers(&mut self) {
        while let Some((gc, obj)) = self.heap.to_finalize.pop() {
            if let Err(e) = self.call_recursive_meta(gc, vec![obj], Some("__gc")) {
                if self.warn {
                    let msg = format!("{}", e);
                    let first_line = match msg.split_once('\n') {
                        Some((first, _)) => first,
                        None => &msg,
                    };
                    let _ = writeln!(stderr(), "Lua warning: error in __gc ({})", first_line);
                }
            }
        }
    }
}

impl Drop for VM {
    fn drop(&mut self) {
        self.heap.closing = true;

        for obj in std::mem::take(&mut self.heap.marked).into_iter() {
            match obj {
                GcObject::Table(tbl) => {
                    if let Some(meta) = tbl.borrow().get_meta() {
                        if let Value::Func(gc) = meta.borrow().get(&Value::str("__gc")) {
                            self.heap.to_finalize.push((gc, Value::Table(tbl.clone())));
                        }
                    }
                }
                GcObject::Func(..) | GcObject::Thread(..) => {}
            }
        }

        self.run_finalizers();
    }
}
