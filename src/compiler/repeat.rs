use crate::ast::Repeat;
use crate::compiler::{offset, Compiler, ScopeType};
use crate::error::Result;
use crate::vm::OpCode;

impl Compiler {
    pub(super) fn compile_repeat(&mut self, repeat: Repeat) -> Result<()> {
        self.scope_enter(ScopeType::Loop);

        // Start
        let start = self.code.current_pc();

        // Block
        self.compile_block(*repeat.block)?;

        // Condition
        let cmp_reg = self.compile_exp(*repeat.cond)?;

        self.scopes.mark_loop_end(self.code.current_pc());

        // Jump to start if condition false
        let off = offset(self.code.current_pc(), start);
        self.code
            .emit(OpCode::JumpIfNot { cmp_reg, off }, repeat.pos_until);

        self.scopes.reg_free(cmp_reg);

        self.scope_leave(ScopeType::Loop, self.code.get_pos_last())?;

        Ok(())
    }
}
