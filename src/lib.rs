// TODO: Add documentation

#[macro_use]
mod error;
mod ast;
mod bitset;
mod compiler;
mod lexer;
mod util;
mod value;
mod vm;

pub use error::{Error, LuaError, Result};
pub use util::read_lua_file;
pub use value::Value;
pub use vm::VM;

pub const LUA_VERSION: (u8, u8) = (5, 4);
