use std::rc::Rc;

use crate::{
    error::{LuaPos, LuaTraces},
    vm::{Code, StackFrame},
    LuaError,
};

use super::FuncDef;

pub const HOOK_CALL: i64 = 0x0001;
pub const HOOK_LINE: i64 = 0x0010;
pub const HOOK_RET: i64 = 0x0100;
pub const HOOK_COUNT: i64 = 0x1000;

// TODO: Create Default impl for this
pub struct Thread {
    pub state: ThreadState,
    pub func: Option<Rc<FuncDef>>,
    pub frames: Vec<StackFrame>,
    pub pc: Option<(Rc<Code>, usize)>,
    pub protected: Option<ProtectedHandler>,
    pub error: Option<(LuaError, LuaPos, LuaTraces)>,
    pub error_close: Option<StackFrame>,
    pub hook_allowed: bool,
    pub hook: Option<Rc<FuncDef>>,
    pub hook_mask: i64,
    pub hook_count: i64,
    pub hook_force_line: bool,
    pub curr_count: i64,
    pub curr_line: i64,
}

#[derive(Clone, Copy, Debug)]
pub enum ThreadState {
    Created,
    Suspended,
    Running,
    Normal,
    Dead,
}

#[derive(Clone, Debug)]
pub enum ProtectedHandler {
    PCall,
    XPCall(Rc<FuncDef>),
}

impl From<Rc<FuncDef>> for Thread {
    fn from(func: Rc<FuncDef>) -> Self {
        Thread {
            state: ThreadState::Created,
            func: Some(func),
            frames: Vec::new(),
            pc: None,
            protected: None,
            error: None,
            error_close: None,
            hook_allowed: true,
            hook: None,
            hook_mask: 0,
            hook_count: 0,
            hook_force_line: false,
            curr_count: 0,
            curr_line: 0,
        }
    }
}
