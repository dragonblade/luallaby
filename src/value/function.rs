use std::{cell::RefCell, fmt, rc::Rc};

use crate::{vm::Code, Result, Value, VM};

#[derive(Clone)]
pub enum FuncDef {
    Builtin(FuncBuiltin),
    BuiltinRaw(FuncBuiltinRaw),
    Defined(FuncClosure),
}

#[derive(Clone)]
pub struct FuncClosure {
    pub regs: usize,
    pub locals_cap: usize,
    pub params: Vec<String>,
    pub ups: Vec<(RefCell<Rc<RefCell<Value>>>, String)>,
    pub varargs: bool,
    pub code: Rc<Code>,
    // Debug information
    pub source: Rc<Vec<u8>>,
    pub linedefined: i64,
    pub lastlinedefined: i64,
}

#[allow(clippy::type_complexity)]
#[derive(Clone)]
pub struct FuncBuiltin {
    pub module: &'static str,
    pub name: &'static str,
    pub func: Rc<dyn Fn(&mut VM) -> Result<Value>>,
}

#[allow(clippy::type_complexity)]
#[derive(Clone)]
pub struct FuncBuiltinRaw {
    pub module: &'static str,
    pub name: &'static str,
    pub func: Rc<dyn Fn(&mut VM) -> Result<CallResult>>,
}

#[derive(Debug)]
pub enum CallResult {
    Continue,
    Return(Value),
    Yield(Value),
}

impl fmt::Debug for FuncDef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            FuncDef::Builtin(func) => {
                write!(f, "builtin:{}", func.name)
            }
            FuncDef::BuiltinRaw(func) => {
                write!(f, "builtin:{}", func.name)
            }
            FuncDef::Defined(func) => write!(f, "defined:{:p}", Rc::as_ptr(&func.code)),
        }
    }
}
