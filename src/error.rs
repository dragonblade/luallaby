use std::fmt::{self, Write};

use thiserror::Error;

use crate::lexer::{Pos, Token};
use crate::value::{Numeric, Value, ValueType};

pub type Result<T> = std::result::Result<T, Error>;

// TODO: Get rid of this
macro_rules! err {
    ( $src:expr, $pos:expr, $typ:expr ) => {
        Err(crate::error::Error::Lua {
            typ: $typ,
            pos: crate::error::LuaPos(Some(match $src.first() {
                Some(b'@' | b'=') => crate::error::LuaTraceSrc::File(
                    String::from_utf8_lossy(&$src[1..]).into_owned(),
                    $pos,
                ),
                _ => crate::error::LuaTraceSrc::Str(
                    String::from_utf8_lossy(&*$src).into_owned(),
                    $pos,
                ),
            })),
            trace: Default::default(),
        })
    };
    ( empty, $typ:expr ) => {
        Err(crate::error::Error::Lua {
            typ: $typ,
            pos: crate::error::LuaPos(Some(crate::error::LuaTraceSrc::Empty)),
            trace: Default::default(),
        })
    };
    ( $typ:expr ) => {
        Err(crate::error::Error::Lua {
            typ: $typ,
            pos: crate::error::LuaPos(None),
            trace: Default::default(),
        })
    };
}

// TODO: Make an error type only for Lua errors, process other error variants into Lua errors accordingly
#[derive(Debug, Error)]
pub enum Error {
    #[error("format error: {0}")]
    Fmt(#[from] std::fmt::Error),
    #[error("IO error: {0}")]
    Io(#[from] std::io::Error),
    #[error("parse int: {0}")]
    ParseInt(#[from] std::num::ParseIntError),
    #[error("parse float: {0}")]
    ParseFloat(#[from] std::num::ParseFloatError),
    #[error("readline: {0}")]
    Readline(#[from] rustyline::error::ReadlineError),
    #[error("deserialization error: {0}")]
    Deserialization(#[from] rmp_serde::decode::Error),
    #[error("serialization error: {0}")]
    Serialization(#[from] rmp_serde::encode::Error),
    #[error("date format: {0}")]
    Time(#[from] time::error::Format),
    #[error("{pos}{typ}\nstack traceback:{trace}")]
    Lua {
        typ: LuaError,
        pos: LuaPos,
        trace: LuaTraces,
    },
}

#[derive(Clone, Debug, Error)]
pub enum LuaError {
    #[error("bad argument #{}{} ({2})", .0+1, .1.as_ref().map(|s| format!(" to '{}'", s)).unwrap_or_default())]
    ArgumentBad(usize, Option<String>, Box<LuaError>),
    #[error("calling '{0}' on bad self ({1})")]
    ArgumentBadSelf(String, Box<LuaError>),
    #[error("wrong number of arguments")]
    ArgumentCount,
    #[error("value expected")]
    ArgumentExpected,
    #[error("got no value")]
    ArgumentNoValue,
    #[error("too many arguments")]
    ArgumentTooMany,
    #[error("assertion failed!")]
    AssertFailed,
    #[error("attempt to assign to const variable '{0}'")]
    AssignToConst(String),
    #[error("attempt to call a {0} value{1}")]
    CallInvalid(ValueType, LuaErrorSrc),
    #[error("invalid option '{0}'")]
    CollectGarbageOption(String),
    #[error("cannot close a normal coroutine")]
    CoroutineCloseNormal,
    #[error("cannot close a running coroutine")]
    CoroutineCloseRunning,
    #[error("cannot resume dead coroutine")]
    CoroutineResumeDead,
    #[error("cannot resume non-suspended coroutine")]
    CoroutineResumeNonSuspended,
    #[error("attempt to yield across a Rust-call boundary")]
    CoroutineYieldRust,
    #[error("attempt to yield from outside a coroutine")]
    CoroutineYieldMain,
    #[error("{0}")]
    CustomNumber(Numeric),
    #[error("{0}")]
    CustomString(String),
    #[error("(error object is a {} value)", .0.value_type())]
    CustomValue(Value),
    #[error("invalid conversion specifier '{0}'")]
    DateFormat(String),
    #[error("invalid date/time")]
    DateInvalid,
    #[error("date result cannot be represented")]
    DateRepresentation,
    #[error("field '{0}' is out-of-bounds")]
    DateTimeFieldBounds(&'static str),
    #[error("field '{0}' is not an integer")]
    DateTimeFieldInteger(&'static str),
    #[error("field '{0}' missing")]
    DateTimeFieldMissing(&'static str),
    #[error("invalid option '{0}'")]
    DebugGetInfoOption(char),
    #[error("level out of range")]
    DebugLocalLevel,
    #[error("invalid option '{0}'")]
    DebugSetHookOption(char),
    #[error("invalid upvalue index")]
    DebugUpvalueJoin,
    #[error("unable to dump given function")]
    DumpInvalid,
    #[error("attempt to divide by zero")]
    DivideByZero,
    #[error("interval is empty")]
    EmptyInterval,
    #[error("error in error handling")]
    ErrorHandler,
    #[error("{0} expected, got {1}")]
    ExpectedType(ValueType, ValueType),
    #[error("number{0} has no integer representation")]
    FloatToInt(LuaErrorSrc),
    #[error("file is already closed")]
    FileAlreadyClosed,
    #[error("invalid option '{0}'")]
    FileBufOpt(String),
    #[error("default input file is closed")]
    FileDefaultInputClosed,
    #[error("default output file is closed")]
    FileDefaultOutputClosed,
    #[error("bad file descriptor")]
    FileDescBad,
    #[error("cannot open file '{0}' ({1})")]
    FileOpen(String, String),
    #[error("invalid mode")]
    FileOpenMode,
    #[error("invalid option")]
    FileSeek(String),
    #[error("invalid format")]
    FileRead,
    #[error("attempt to use a closed file")]
    FileUseClosed,
    #[error("bad 'for' {0} value ({1})")]
    ForBadValue(&'static str, Box<LuaError>),
    #[error("'for' step is zero")]
    ForStepZero,
    #[error("<goto {0}> jumps into the scope of local '{1}'")]
    GotoJumpInLocal(String, String),
    #[error("no visible label '{0}' for <goto> at line {1}")]
    GotoLabelNotFound(String, i64),
    #[error("label '{0}' already defined on line {1}")]
    GotoLabelRepeat(String, i64),
    #[error("attempt to index a {0} value ({2} '{1}')")]
    IndexNonTable(ValueType, String, &'static str),
    #[error("interrupted")]
    Interrupt,
    #[error("index out of range")]
    InvalidIndex,
    #[error("'{0}' must be a {1}")]
    InvalidType(&'static str, ValueType),
    #[error("decimal escape too large near '{0}'")]
    LexDecimalEscape(String),
    #[error("hexadecimal digit expected near '{0}'")]
    LexHexDigit(String),
    #[error("unfinished long string near <eof>")]
    LexLongString,
    #[error("invalid long string delimiter near '{0}'")]
    LexLongStringDelim(String),
    #[error("missing '{0}' near '{1}'")]
    LexMissing(char, String),
    #[error("malformed number near '{0}'")]
    LexNumber(String),
    #[error("unfinished string near <eof>")]
    LexString,
    #[error("invalid escape sequence near '{0}'")]
    LexStringEscape(String),
    #[error("syntax error near")]
    LexStringNewline,
    #[error("unexpected symbol near '<\\{}>'", *.0 as u8)]
    LexSymbol(char),
    #[error("UTF-8 value too large near '{0}'")]
    LexUtf8TooLarge(String),
    #[error("attempt to load a {0} chunk (mode is '{1}')")]
    LoadMode(&'static str, String),
    #[error("reader function must return string")]
    LoadReader,
    #[error("variable '{0}' got a non-closable value")]
    LocalNonClose(String),
    #[error("invalid key to 'next'")]
    NextInvalidKey,
    #[error("zero")]
    MathFmodZero,
    #[error("cannot change a protected metatable")]
    MetatableProtected,
    #[error("'__index' chain too long; possible loop")]
    MetatableIndex,
    #[error("'__newindex' chain too long; possible loop")]
    MetatableNewIndex,
    #[error("module '{0}' not found")]
    ModuleNotFound(String),
    #[error("attempt to perform arithmetic on a {0} value{1}")]
    OperatorArith(ValueType, LuaErrorSrc),
    #[error("attempt to perform bitwise operation on a {0} value")]
    OperatorBitwise(ValueType),
    #[error("attempt to compare {0} with {1}")]
    OperatorCompare(ValueType, ValueType),
    #[error("attempt to compare two {0} values")]
    OperatorCompareSame(ValueType),
    #[error("attempt to concatenate a {0} value")]
    OperatorConcat(ValueType),
    #[error("attempt to get length of a {0} value")]
    OperatorLength(ValueType),
    #[error("format asks for alignment not power of 2")]
    PackAlign,
    #[error("invalid format option '{0}'")]
    PackFormatOption(char),
    #[error("integer overflow")]
    PackIntOverflow,
    #[error("invalid next option for option 'X'")]
    PackInvalidX,
    #[error("missing size for format option 'c'")]
    PackMissingSize,
    #[error("integral size ({0}) out of limits [1,16]")]
    PackSizeLimit(usize),
    #[error("format result too large")]
    PackSizeTooLarge,
    #[error("variable-length format")]
    PackSizeVariableLength,
    #[error("string longer than given size")]
    PackStringC,
    #[error("string length does not fit in given size")]
    PackStringLength,
    #[error("attempt to perform 'n%0'")]
    RemainderByZero,
    #[error("C stack overflow")]
    StackOverflow,
    #[error("invalid conversion specification")]
    StringFormatInvalidSpec,
    #[error("value has no literal form")]
    StringFormatNoLiteral,
    #[error("no value found")]
    StringFormatNoValue,
    #[error("specifier '%q' cannot have modifiers")]
    StringFormatQ,
    #[error("invalid format (too long)")]
    StringFormatTooLong,
    #[error("invalid use of '%'")]
    StringGSubInvalidEscape,
    #[error("invalid argument")]
    StringGSubArgument,
    #[error("invalid replacement value (a {0})")]
    StringGSubReplacementValue(ValueType),
    #[error("malformed pattern (missing arguments to '%b')")]
    StringPatternBalance,
    #[error("pattern too complex")]
    StringPatternDepth,
    #[error("malformed pattern (ends with '%')")]
    StringPatternEscape,
    #[error("missing '[' after '%f' in pattern")]
    StringPatternFrontier,
    #[error("invalid capture index %{0}")]
    StringPatternInvalidCapture(u8),
    #[error("invalid pattern capture")]
    StringPatternNoCapture,
    #[error("malformed pattern (missing ']'")]
    StringPatternSet,
    #[error("unfinished capture")]
    StringPatternUnfinishedCapture,
    #[error("resulting string too large")]
    StringTooLarge,
    #[error("string contains zeros")]
    StringZeros,
    #[error("too many {0}{}", .1.map(|l| if l > 0 { format!(" in function at line {}", l) } else { " in main function".to_string()} ).unwrap_or_default())]
    SyntaxLimit(&'static str, Option<i64>),
    #[error("invalid value (nil) at index {0} in table for 'concat'")]
    TableConcat(i64),
    #[error("position out of bounds")]
    TableInsert,
    #[error("invalid table index")]
    TableIndex,
    #[error("invalid table field")]
    TableFieldParse,
    #[error("object length is not an integer")]
    TableLength,
    #[error("too many elements to move")]
    TableMoveCount,
    #[error("destination wrap around")]
    TableMoveWrap,
    #[error("invalid order function for sorting")]
    TableSortFunc,
    #[error("array too big")]
    TableSortSize,
    #[error("too many results to unpack")]
    TableUnpack,
    #[error("'__tostring' must return a string")]
    ToStringReturnType,
    #[error("bad binary format")]
    UndumpInvalid,
    #[error("syntax error near <eof>, expected")]
    UnexpectedEof,
    #[error("'{1}' expected (to close '{0}' at line {2}) near <eof>")]
    UnexpectedEofBalanced(char, char, i64),
    #[error("unexpected symbol near '{0}'")]
    UnexpectedToken(Token),
    #[error("unknown attribute '{0}'")]
    UnknownAttribute(String),
    #[error("{0}-byte integer does not fit into Lua integer")]
    UnpackIntOverflow(usize),
    #[error("initial position out of string")]
    UnpackPosition,
    #[error("data string too short")]
    UnpackStringS,
    #[error("unfinished string for format 'z'")]
    UnpackStringZ,
    #[error("data string too short")]
    UnpackTooShort,
    #[error("invalid UTF-8 code")]
    Utf8Invalid,
    #[error("initial position is continuation byte")]
    Utf8IsContByte,
    #[error("position out of bounds")]
    Utf8OutOfBounds,
    #[error("value out of range")]
    ValueRange,
}

#[derive(Clone, Debug)]
pub enum LuaErrorSrc {
    None,
    Field(String),
    Global(String),
    Local(String),
    Metamethod(String),
    Method(String),
    Up(String),
}

#[derive(Clone, Debug)]
pub struct LuaPos(pub Option<LuaTraceSrc>);

#[derive(Clone, Debug, Default)]
pub struct LuaTraces(Vec<LuaTrace>);

#[derive(Clone, Debug, PartialEq)]
pub enum LuaTrace {
    Rust(String),
    Lua(LuaTraceSrc, LuaTraceScope),
    Tail,
}

#[derive(Clone, Debug, PartialEq)]
pub enum LuaTraceSrc {
    Empty,
    File(String, Pos),
    Str(String, Pos),
}

#[derive(Clone, Debug, PartialEq)]
pub enum LuaTraceScope {
    Function(String, i64),
    Main,
    Source(&'static str, String),
}

impl Error {
    pub fn from_lua(typ: LuaError) -> Self {
        Error::Lua {
            typ,
            pos: LuaPos(None),
            trace: Default::default(),
        }
    }

    pub fn map_lua_error<F>(self, func: F) -> Self
    where
        F: FnOnce(LuaError) -> LuaError,
    {
        match self {
            Error::Lua { typ, pos, trace } => Error::Lua {
                typ: func(typ),
                pos,
                trace,
            },
            e => e,
        }
    }

    pub fn traces(self, traces: LuaTraces) -> Self {
        match self {
            Error::Lua { typ, pos, .. } => Error::Lua {
                typ,
                pos: match pos.0 {
                    Some(pos) => LuaPos(Some(pos)),
                    None => match traces.0.first() {
                        Some(LuaTrace::Lua(src, _)) => LuaPos(Some(src.clone())),
                        Some(LuaTrace::Rust(..)) => match traces.0.get(1) {
                            Some(LuaTrace::Lua(src, _)) => LuaPos(Some(src.clone())),
                            _ => LuaPos(None),
                        },
                        _ => LuaPos(None),
                    },
                },
                trace: traces,
            },
            e => e,
        }
    }

    pub fn has_trace(&self) -> bool {
        match self {
            Error::Lua { trace, .. } => !trace.0.is_empty(),
            _ => true,
        }
    }

    pub fn to_value(&self) -> Value {
        match self {
            Error::Lua { typ, pos, .. } => match typ {
                LuaError::CustomNumber(n) => Value::Number(n.clone()),
                LuaError::CustomValue(v) => v.clone(),
                typ => Value::string(format!("{}{}", pos, typ)),
            },
            e => Value::string(format!("{}", e)),
        }
    }

    pub fn into_value(self) -> Value {
        match self {
            Error::Lua { typ, pos, .. } => match typ {
                LuaError::CustomNumber(n) => Value::Number(n),
                LuaError::CustomValue(v) => v,
                typ => Value::string(format!("{}{}", pos, typ)),
            },
            e => Value::string(format!("{}", e)),
        }
    }
}

impl LuaError {
    pub fn into_value(self) -> Value {
        match self {
            LuaError::CustomValue(v) => v,
            e => Value::string(format!("{}", e)),
        }
    }
}

impl fmt::Display for LuaErrorSrc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LuaErrorSrc::None => Ok(()),
            LuaErrorSrc::Field(name) => write!(f, " (field '{}')", name),
            LuaErrorSrc::Global(name) => write!(f, " (global '{}')", name),
            LuaErrorSrc::Local(name) => write!(f, " (local '{}')", name),
            LuaErrorSrc::Metamethod(name) => write!(f, " (metamethod '{}')", name),
            LuaErrorSrc::Method(name) => write!(f, " (method '{}')", name),
            LuaErrorSrc::Up(name) => write!(f, " (upvalue '{}')", name),
        }
    }
}

impl fmt::Display for LuaPos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(src) = &self.0 {
            if !matches!(src, LuaTraceSrc::Empty) {
                // Suppress column number, as lua test suite does not want to see it
                write!(f, "{:#} ", src)?;
            }
        }
        Ok(())
    }
}

impl LuaTraces {
    pub fn trace(&mut self, trace: LuaTrace) {
        self.0.push(trace);
    }

    pub fn truncate(&mut self, count: usize) {
        self.0 = self.0.split_off(count.min(self.0.len()));
    }
}

impl fmt::Display for LuaTraceSrc {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        const LEN_MAX: usize = 59;
        const DOTS: &str = "...";

        const STR_PRE: &str = "[string \"";
        const STR_POST: &str = "\"]";
        const STR_LEN: usize = STR_PRE.len() + STR_POST.len();

        match self {
            LuaTraceSrc::Empty => {}
            LuaTraceSrc::File(filename, pos) => {
                if filename.len() > LEN_MAX {
                    f.write_str(&filename[..(LEN_MAX - DOTS.len())])?;
                    f.write_str(DOTS)?;
                } else {
                    f.write_str(filename)?;
                }
                f.write_char(':')?;
                write!(f, "{}:", pos.line())?;
                if !f.alternate() {
                    write!(f, "{}:", pos.col())?;
                }
            }
            LuaTraceSrc::Str(str, pos) => {
                f.write_str(STR_PRE)?;
                let mut lines = str.lines();
                if let Some(line) = lines.next() {
                    let line_next = lines.next().is_some();
                    if line.len() + STR_LEN > LEN_MAX || line_next {
                        f.write_str(&line[..(LEN_MAX - (STR_LEN + DOTS.len())).min(line.len())])?;
                        f.write_str(DOTS)?;
                    } else {
                        f.write_str(line)?;
                        if line_next {
                            f.write_str(DOTS)?;
                        }
                    }
                }
                f.write_str(STR_POST)?;
                f.write_char(':')?;
                write!(f, "{}:", pos.line())?;
                if !f.alternate() {
                    write!(f, "{}:", pos.col())?;
                }
            }
        }
        Ok(())
    }
}

impl fmt::Display for LuaTraces {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        const MAX_TOP: usize = 10;
        const MAX_BOT: usize = 11;
        const MAX_LINES: usize = MAX_TOP + MAX_BOT;
        if self.0.len() > MAX_LINES {
            let mut iter = self.0.iter();
            for _ in 0..MAX_TOP {
                write!(f, "\n\t{}", iter.next().unwrap())?;
            }
            write!(f, "\n\t... (skipping {} levels)", self.0.len() - MAX_LINES)?;
            for trace in iter.skip(self.0.len() - MAX_LINES) {
                write!(f, "\n\t{}", trace)?;
            }
        } else {
            for trace in self.0.iter() {
                write!(f, "\n\t{}", trace)?;
            }
        }
        Ok(())
    }
}

impl fmt::Display for LuaTrace {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LuaTrace::Rust(name) => write!(f, "[Rust]: in function '{}'", name),
            LuaTrace::Lua(src, scope) => {
                write!(f, "{}", src)?;
                match scope {
                    LuaTraceScope::Function(src, line) => {
                        write!(f, " in function <{}:{}>", src, line)
                    }
                    LuaTraceScope::Main => write!(f, " in main chunk"),
                    LuaTraceScope::Source(typ, func) => write!(f, " in {} '{}'", typ, func),
                }
            }
            LuaTrace::Tail => write!(f, "(...tail calls...)"),
        }
    }
}
