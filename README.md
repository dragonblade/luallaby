# luallaby

[![Crates.io](https://img.shields.io/crates/v/luallaby)](https://crates.io/crates/luallaby)
[![docs.rs](https://img.shields.io/docsrs/luallaby)](https://docs.rs/luallaby)
[![Crates.io](https://img.shields.io/crates/l/luallaby)](LICENSE)

**This project is a work in progress. Please do not use this (yet) if you need a reasonably fast Lua interpreter.**

A Lua compiler and interpreter fully written in Rust.
This project aims to provide a fully compliant Lua implementation including interoperability between Lua and Rust.

## Things to do and implement

A list of features to be implemented, in somewhat particular order:

- [x] Full language syntax support
- [ ] Proper error handling with trace+context (not panicking on every problem)
- [ ] Rust-Lua interoperability
- [x] Standard libraries
    - [x] Basic library
    - [x] Coroutine library
    - [x] Package library
    - [x] String manipulation
    - [x] Basic UTF-8 support
    - [x] Table manipulation
    - [x] Mathematical functions
    - [x] Input and output
    - [x] Operating system facilities
    - [x] Debug facilities
- [x] Garbage collector
- [x] Passing the Lua test suite
- [ ] Benchmarks vs other compilers
    - [ ] Reasonable performance
- [x] REPL

## Testing

This crate has its own test suite that mainly contains basic unit tests or sanity checks to verify if all parts of the language and standard library are in at least a semi-working condition.
These sanity checks can be run with:

```
cargo test
```

A copy of the Lua test suite has also been included in this repository.
These tests can be run by running them as regular Lua files `cargo run -- <file>.lua`, the test is considered passed if the output ends with "OK" and the interpreter has returned without error.

The Lua test suite has also been included in the `cargo test` suite, but has been marked as ignored.
To include the Lua test suite with the sanity checks run:

```
cargo test -- --include-ignored
```

## Usage

TODO:

## License

`luallaby` is distributed under the terms of the MIT license.

See the [LICENSE](LICENSE) file for more information.
