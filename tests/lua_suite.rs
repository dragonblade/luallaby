macro_rules! test_lua {
    ( $name:ident, $filename:literal ) => {
        #[ignore]
        #[test]
        fn $name() {
            let output = std::process::Command::new("script")
                .arg("-qec")
                .arg(format!("{} {}", env!("CARGO_BIN_EXE_luallaby"), $filename))
                .arg("/dev/null")
                .current_dir("lua-5.4.6-tests")
                .output()
                .expect("failed to execute process");
            let out = String::from_utf8_lossy(&output.stdout).into_owned();
            println!("{}", out);
            assert!(output.status.success());
        }
    };
}

test_lua!(attrib, "attrib.lua");
test_lua!(bitwise, "bitwise.lua");
test_lua!(calls, "calls.lua");
test_lua!(closure, "closure.lua");
test_lua!(constructs, "constructs.lua");
test_lua!(coroutine, "coroutine.lua");
test_lua!(cstack, "cstack.lua");
test_lua!(db, "db.lua");
test_lua!(errors, "errors.lua");
test_lua!(events, "events.lua");
test_lua!(files, "files.lua");
test_lua!(gc, "gc.lua");
test_lua!(gengc, "gengc.lua");
test_lua!(goto, "goto.lua");
test_lua!(literals, "literals.lua");
test_lua!(locals, "locals.lua");
test_lua!(main, "main.lua");
test_lua!(math, "math.lua");
test_lua!(nextvar, "nextvar.lua");
test_lua!(pm, "pm.lua");
test_lua!(sort, "sort.lua");
test_lua!(strings, "strings.lua");
test_lua!(tpack, "tpack.lua");
test_lua!(utf8, "utf8.lua");
test_lua!(vararg, "vararg.lua");
test_lua!(verybig, "verybig.lua");
