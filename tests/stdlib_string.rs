use luallaby::LuaError;

mod macros;

test!(
    string_byte,
    "
print(string.byte('ABC'))
print(string.byte('ABC', 2))
print(string.byte('ABC', 2, 1))
print(string.byte('', 1, 3))
print(string.byte('ABC', 0, 3))
print(string.byte('ABC', '1', 4))
print(string.byte('ABC', 1, '4.0'))
print(string.byte('ABC', 0, 4))
print(string.byte('☃', 1, 10))
print(string.byte(4.4, 1, 3))
",
    "
65
66


65\t66\t67
65\t66\t67
65\t66\t67
65\t66\t67
226\t152\t131
52\t46\t52
"
);

test!(
    string_char,
    "
print(string.char(65))
print(string.char('65', 66, '67.0'))
",
    "
A
ABC
"
);

test_err!(string_char_fail_1, "string.char(256)", LuaError::ValueRange);
test_err!(
    string_char_fail_2,
    "string.char({})",
    LuaError::ExpectedType(..)
);

test!(
    string_find,
    "
print(string.find(\"abc\", \"bc\"))
print(string.find(\"abc\", \"[bc]\"))
print(string.find(\"some, 42 is a cool number, other\", \"%d* is a cool number\"))
",
    "
2\t3
2\t2
7\t25
"
);

test!(string_format, "print(string.format(\"%%\"))", "%");

test!(
    string_format_c,
    "
print(string.format(\"%c\", 65))
print(string.format(\"%3c\", 65))
print(string.format(\"%-3c\", 65))
print(string.format(\"%+c\", 65))
print(string.format(\"% c\", 65))
print(string.format(\"%#c\", 65))
",
    "
A
  A
A  \n\
A
A
A
"
);

test!(
    string_format_s,
    "
print(string.format(\"Hello %s!\", \"world\"))
print(string.format(\"Hello %s!\", true))
print(string.format(\"%5s\", \"abc\"))
print(string.format(\"%-5s\", \"abc\"))
print(string.format(\"%+s\", \"abc\"))
print(string.format(\"% s\", \"abc\"))
print(string.format(\"%#s\", \"abc\"))
print(string.format(\"%.2s\", \"abc\"))
print(string.format(\"%3.2s\", \"abc\"))
",
    "
Hello world!
Hello true!
  abc
abc  \n\
abc
abc
abc
ab
 ab
"
);

test!(
    string_format_di,
    "
print(string.format(\"%d%i\", 1, 2))
print(string.format(\"%3d%3i\", 1, 2))
print(string.format(\"%-3d%-3i\", 1, 2))
print(string.format(\"%+d%+i\", 1, 2))
print(string.format(\"% d% i\", 1, -2))
print(string.format(\"% d% i\", -1, 2))
print(string.format(\"%+ d%+ i\", 1, 2))
print(string.format(\"%03d%03i\", 1, 2))
print(string.format(\"%.3d%.3i\", 1, 2))
print(string.format(\"%.3d%.3i\", -1, -2))
print(string.format(\"%2.0d%2.0i\", 0, 0))
print(string.format(\"%-03d%-03i\", 1, 2))
",
    "
12
  1  2
1  2  \n\
+1+2
 1-2
-1 2
+1+2
001002
001002
-001-002
    \n\
1  2  \n\
"
);

test!(
    string_format_oux,
    "
print(string.format(\"%o,%u,%x,%X\", 17, 77, 171, 172))
print(string.format(\"%4o,%4u,%4x,%4X\", 17, 77, 171, 172))
print(string.format(\"%-4o,%-4u,%-4x,%-4X\", 17, 77, 171, 172))
print(string.format(\"%+o,%+u,%+x,%+X\", 17, 77, 171, 172))
print(string.format(\"% o,% u,% x,% X\", 17, 77, 171, 172))
print(string.format(\"%o,%u,%x,%X\", 17, 77, 171, 172))
print(string.format(\"%#o,%u,%#x,%#X\", 17, 77, 171, 172))
print(string.format(\"%.0o\", 0))
print(string.format(\"%#o\", 0))
print(string.format(\"%#.0o\", 0))
print(string.format(\"%#.3o\", 0))
print(string.format(\"%#4.3o\", 0))
print(string.format(\"%03o,%03u,%03x,%03X\", 17, 77, 171, 172))
print(string.format(\"%.3o,%.3u,%.3x,%.3X\", 17, 77, 171, 172))
print(string.format(\"%.0o,%.0u,%.0x,%.0X\", 0, 0, 0, 0))
print(string.format(\"%o,%u,%x,%X\", -1, -1, -1, -1))
",
    "
21,77,ab,AC
  21,  77,  ab,  AC
21  ,77  ,ab  ,AC  \n\
21,77,ab,AC
21,77,ab,AC
21,77,ab,AC
021,77,0xab,0XAC

0
0
000
 000
021,077,0ab,0AC
021,077,0ab,0AC
,,,
1777777777777777777777,18446744073709551615,ffffffffffffffff,FFFFFFFFFFFFFFFF
"
);

test!(
    string_format_a,
    "
print(string.format(\"%a,%A\", 7, 7))
print(string.format(\"%a,%A\", -7, -7))
print(string.format(\"%a,%A\", 10, 10))
print(string.format(\"%a,%A\", -0, -0))
print(string.format(\"%a,%A\", 1.0e-308, 1.0e-308)) -- subnormal float
print(string.format(\"%a,%A\", 0/0, 0/0)) -- NaN
print(string.format(\"%a,%A\", -1/0, -1/0)) -- -Inf
print(string.format(\"%7a,%7A\", 1, 1))
print(string.format(\"%-7a,%-7A\", 1, 1))
print(string.format(\"%+a,%+A\", 1, 1))
print(string.format(\"% a,% A\", 1, 1))
print(string.format(\"%#a,%#A\", 1, 1))
print(string.format(\"%07a,%07A\", 1, 1))
print(string.format(\"%.3a,%.3A\", 1.4, 1.4))
print(string.format(\"% 07a,% 07A\", 1, 1))
",
    "
0x1.cp2,0X1.CP2
-0x1.cp2,-0X1.CP2
0x1.4p3,0X1.4P3
0x0p0,0X0P0
0x0.730d67819e8d2p-1022,0X0.730D67819E8D2P-1022
-nan,-NAN
-inf,-INF
  0x1p0,  0X1P0
0x1p0  ,0X1P0  \n\
+0x1p0,+0X1P0
 0x1p0, 0X1P0
0x1.p0,0X1.P0
0x001p0,0X001P0
0x1.666p0,0X1.666P0
 0x01p0, 0X01P0
"
);

test!(
    string_format_ef,
    "
print(string.format(\"%e,%E,%f\", 1024, 1024, 1024))
print(string.format(\"%e,%E,%f\", 2.3, 2.3, 2.3))
print(string.format(\"%12e,%12E,%12f\", 2.3, 2.3, 2.3))
print(string.format(\"%-12e,%-12E,%-12f\", 2.3, 2.3, 2.3))
print(string.format(\"%+e,%+E,%+f\", 2.3, 2.3, 2.3))
print(string.format(\"% e,% E,% f\", 2.3, 2.3, 2.3))
print(string.format(\"%#.0e,%#.0E,%#.0f\", 2, 2, 2))
print(string.format(\"%012e,%012E,%012f\", 2.3, 2.3, 2.3))
print(string.format(\"%.3e,%.3E,%.3f\", 2.30244, 2.30244, 2.30244))
",
    "
1.024000e+03,1.024000E+03,1024.000000
2.300000e+00,2.300000E+00,2.300000
2.300000e+00,2.300000E+00,    2.300000
2.300000e+00,2.300000E+00,2.300000    \n\
+2.300000e+00,+2.300000E+00,+2.300000
 2.300000e+00, 2.300000E+00, 2.300000
2.e+00,2.E+00,2.
2.300000e+00,2.300000E+00,00002.300000
2.302e+00,2.302E+00,2.302    
"
);

test!(
    string_format_g,
    "
print(string.format(\"%g,%G\", 999999, 999999))
print(string.format(\"%g,%G\", 1000000, 1000000))
print(string.format(\"%g,%G\", 2000.000, 2000.000))
print(string.format(\"%g,%G\", 2000000.000, 2000000.000))
print(string.format(\"%.6g,%.6G\", 200.1028, 200.1028))
print(string.format(\"%.7g,%.7G\", 200.1028, 200.1028))
print(string.format(\"%g,%G\", 2140000.000, 2140000.000))
print(string.format(\"%g,%G\", 2144656.000, 2144656.000))
print(string.format(\"%g,%G\", 0.0004, 0.0004))
print(string.format(\"%g,%G\", 0.00004, 0.00004))
print(string.format(\"%#g,%#G\", 2, 2))
print(string.format(\"%#g,%#G\", 2000000, 2000000))
print(string.format(\"%#.3g,%#.3G\", 2000000, 2000000))
print(string.format(\"%#08g,%#08G\", 200, 200))
print(string.format(\"%08g,%08G\", 200, 200))
print(string.format(\"%4g,%4G\", 200, 200))
print(string.format(\"%-4g,%-4G\", 200, 200))
print(string.format(\"%+g,%+G\", 200, 200))
print(string.format(\"% g,% G\", 200, 200))
",
    "
999999,999999
1e+06,1E+06
2000,2000
2e+06,2E+06
200.103,200.103
200.1028,200.1028
2.14e+06,2.14E+06
2.14466e+06,2.14466E+06
0.0004,0.0004
4e-05,4E-05
2.00000,2.00000
2.00000e+06,2.00000E+06
2.00e+06,2.00E+06
0200.000,0200.000
00000200,00000200
 200, 200
200 ,200 \n\
+200,+200
 200, 200
"
);

test_regex!(
    string_format_p,
    "
print(string.format(\"%p\", 0))
print(string.format(\"%p\", function () end))
print(string.format(\"%p\", \"\"))
print(string.format(\"%p\", {}))
print(string.format(\"%+p\", {}))
print(string.format(\"%17p\", {}))
print(string.format(\"%-17p\", {}))
print(string.format(\"% p\", {}))
print(string.format(\"%017p\", {}))
",
    "
\\(null\\)
0x([a-fA-F0-9])+
0x([a-fA-F0-9])+
0x([a-fA-F0-9])+
\\+0x([a-fA-F0-9])+
( )+0x([a-fA-F0-9])+
0x([a-fA-F0-9])+( )+
 0x([a-fA-F0-9])+
0x(0)+([a-fA-F0-9])+
"
);

test!(
    string_format_q,
    "
print(string.format(\"%q\", nil))
print(string.format(\"%q\", true))
print(string.format(\"%q\", 10.0))
print(string.format('%q', 'a string with \"quotes\" and \\n new line'))
",
    "
nil
true
0x1.4p3
\"a string with \\\"quotes\\\" and \\
 new line\"
"
);

test_err!(
    string_format_q_no_mod_1,
    "string.format(\"%+q\", nil)",
    LuaError::StringFormatQ
);
test_err!(
    string_format_q_no_mod_2,
    "string.format(\"%-q\", nil)",
    LuaError::StringFormatQ
);
test_err!(
    string_format_q_no_mod_3,
    "string.format(\"% q\", nil)",
    LuaError::StringFormatQ
);
test_err!(
    string_format_q_no_mod_4,
    "string.format(\"%3q\", nil)",
    LuaError::StringFormatQ
);
test_err!(
    string_format_q_no_mod_5,
    "string.format(\"%03q\", nil)",
    LuaError::StringFormatQ
);
test_err!(
    string_format_q_no_mod_6,
    "string.format(\"%.3q\", nil)",
    LuaError::StringFormatQ
);

test_err!(
    string_format_invalid,
    "string.format(\"%\", 2)",
    LuaError::StringFormatInvalidSpec
);

test_err!(
    string_format_args,
    "string.format(\"%c\")",
    LuaError::StringFormatNoValue
);

test!(
    string_gsub,
    "
print(string.gsub('hello world', '(%w+)', '%1 %1'))
print(string.gsub('hello world', '%w+', '%0 %0', 1))
print(string.gsub('hello world from Lua', '(%w+)%s*(%w+)', '%2 %1'))
",
    "
hello hello world world\t2
hello hello world\t1
world hello Lua from\t2
"
);

test!(
    string_gsub_table,
    "
local t = {name='lua', version='5.4'}
print(string.gsub('$name-$version.tar.gz', '%$(%w+)', t))
",
    "
lua-5.4.tar.gz\t2
"
);

test!(
    string_gsub_function,
    "
print(string.gsub('4+5 = $return 4+5$', '%$(.-)%$', function (s) return load(s)() end))
string.gsub('443', '%d$', function(...) print(...) end)
string.gsub('443', '%d(%d)$', function(...) print(...) end)
string.gsub('443', '(%d)(%d)$', function(...) print(...) end)
",
    "
4+5 = 9\t1
3
3
4\t3
"
);

test!(
    string_gsub_function_builtin,
    "
print(string.gsub('4+5 = $return 4+5$', '%$(.-)%$', print))
",
    "
return 4+5
4+5 = $return 4+5$\t1
"
);

test!(
    string_gsub_table_no_captures,
    "
print(string.gsub('XYZ', '[XYZ]', {X=1,Y=2,Z=3}))
",
    "
123\t3
"
);

test!(
    string_len,
    "
print(string.len('abc'))
print(string.len('a\\000bc\\000'))
print(string.len(4))
print(string.len(4.2))
",
    "
3
5
1
3
"
);

test!(
    string_rep,
    "
print(string.rep(3, 4))
print(string.rep(3, '4'))
print(string.rep(3, '4.0'))
print(string.rep(3.3, 2))
print(string.rep('ab', 3, 4))
print(string.rep(3, 0))
print(string.rep(3, -1))
print(string.rep('true', 3, ','))
",
    "
3333
3333
3333
3.33.3
ab4ab4ab


true,true,true
"
);

test!(
    string_reverse,
    "
a = '123'
b = a:reverse()
print(a, b)
",
    "
123\t321
"
);
