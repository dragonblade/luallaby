use luallaby::LuaError;

mod macros;

test!(
    basic_assert_pass,
    "
print(assert(0, \"failed\"))
",
    "
0\tfailed
"
);

test_err!(
    basic_assert_fail,
    "assert(nil, \"hehe\")",
    LuaError::CustomValue(..)
);

test!(
    basic_load,
    "
f = load(\"a = 'Hello world!'; print(a, a)\")
f()
",
    "
Hello world!\tHello world!
"
);

test!(
    basic_getmetatable,
    "
local t = {}
assert(not getmetatable(t))
local meta = {}
assert(setmetatable(t, meta) == t)
assert(getmetatable(t) == meta)

local meta_meta = { __metatable = 4 }
setmetatable(t, meta_meta)

assert(getmetatable(t) == 4)
",
    ""
);

test!(
    basic_pcall,
    "
function f()
    print(\"Hello world!\")
    error()
    print(\"Don't come here\")
end
pcall(f)
",
    "
Hello world!
"
);

test!(
    basic_pcall_pass,
    "
function f()
    print('Hello world!')
end
function g()
    return 5
end
pcall(f)
p, r = pcall(g)
print(r + 4)
print(pcall(print))
print(pcall(math.abs, -5))
",
    "
Hello world!
9

true
true\t5
"
);

test!(
    basic_pcall_double,
    "
function f()
    error(5)
end
function g()
    return 5
end
print(pcall(pcall, f))
print(pcall(pcall, g))
",
    "
true\tfalse\t5
true\ttrue\t5
"
);

test!(
    basic_pcall_nested,
    "
function f()
    error(5)
end
function g()
    return 5
end
function h()
    print(pcall(f))
    print(pcall(g))
end
print(pcall(h))
print(pcall(math.abs, \"haha\"))
",
    "
false\t5
true\t5
true
false\tbad argument #1 to 'math.abs' (number expected, got string)
"
);

test!(
    basic_pcall_filled_stack,
    "
function f()
    local a, b, c = 4, 5, error()
end
a, b, c = 1, pcall(f), 2
print(a, b, c)
",
    "
1\tfalse\t2
"
);

test!(
    basic_pcall_across_rust,
    "
print(pcall(string.gsub, \"abc\", \"abc\", function(s) error(42) end))
",
    "
false\t42
"
);

test!(
    basic_select,
    "
print(select(1, 1, 2, 3, 4))
print(select('-2', 1, 2, 3, 4))
print(select('2.0', 1, 2, 3, 4))
print(select('-2.0', 1, 2, 3, 4))
print(select(5, 1, 2, 3, 4))
print(select('#', 1, 2, 3, 4))
",
    "
1\t2\t3\t4
3\t4
2\t3\t4
3\t4

4
"
);

test_err!(
    basic_select_fail_1,
    "select(4.1, 1, 2, 3, 4)",
    LuaError::FloatToInt(..)
);
test_err!(
    basic_select_fail_2,
    "select(0, 1, 2, 3, 4)",
    LuaError::InvalidIndex
);
test_err!(
    basic_select_fail_3,
    "select(-5, 1, 2, 3, 4)",
    LuaError::InvalidIndex
);

test_err!(
    basic_setmetatable_fail,
    "
local t = {}
local meta = { __metatable = function () end }
setmetatable(t, meta)
local newmeta = {}
setmetatable(t, newmeta)
",
    LuaError::MetatableProtected
);

test!(
    basic_tonumber,
    "
print(tonumber('4', 3))
print(tonumber('10', 16))
print(tonumber('10.2', 16))
print(tonumber('10.2'))
print(tonumber('0x10.2'))
print(tonumber('0x10'))
print(tonumber(' 10 '))
",
    "
nil
16
nil
10.2
16.125
16
10
"
);

test_err!(
    basic_tonumber_fail_1,
    "print(tonumber(4, 3))",
    LuaError::ExpectedType(..)
);
test_err!(
    basic_tonumber_fail_2,
    "print(tonumber('4', '3F'))",
    LuaError::ExpectedType(..)
);

test_regex!(
    basic_tostring,
    "
a = nil
b = true
c = 0.1
d = \"a\"
e = {}
function f()
end
print(a)
print(b)
print(c)
print(d)
print(e)
print(f)
",
    r"
nil
true
0.1
a
table: 0x([a-fA-F0-9])+
function: 0x([a-fA-F0-9])+
"
);

test!(
    basic_xpcall,
    "
print(xpcall(error, function(err) print('handle', err); return 33 end, 42))
",
    "
handle\t42
false\t33
"
);

test!(
    basic_xpcall_pass,
    "
print(xpcall(function(i) print(i); return i end, function() print('handle') end, 42))
",
    "
42
true\t42
"
);

test!(
    basic_xpcall_nested,
    "
function h(err)
    print('handle', err)
    return err
end
function f()
    error(42)
end
function g()
    print(xpcall(f, h))
    return 33
end
print(xpcall(g, function() print('never called') end))
",
    "
handle\t42
false\t42
true\t33
"
);

test!(
    basic_xpcall_nested_fail,
    "
function h(err)
    print('inner', err)
    return 22
end
function f()
    error(42)
end
function g()
    print(xpcall(f, h))
    error(33)
end
print(xpcall(g, function(e) print('outer', e); return 11 end))
",
    "
inner\t42
false\t22
outer\t33
false\t11
"
);

test!(
    basic_xpcall_fail_handler,
    "
print(xpcall(error, function() error(33) end, 42))
",
    "
false\terror in error handling
"
);

test!(
    basic_xpcall_fail_handler_few_times,
    "
print(xpcall(
    error,
    function(err)
        print('handle', err)
        if err == 42 then
            error(33)
        elseif err == 33 then
            error(22)
        elseif err == 22 then
            return 11
        end
    end,
    42
))
",
    "
handle\t42
handle\t33
handle\t22
false\t11
"
);

test!(
    basic_xpcall_fail_closing,
    "
function f()
    local x<close> = setmetatable({}, {__close = function() error(33) end})
    error(42)
end
print(xpcall(f, function (err) print('handle', err); return err end, 42))
",
    "
handle\t42
handle\t33
false\t33
"
);

test!(
    basic_pcall_nil,
    "
print(pcall(nil, 42))    
",
    "
false\tattempt to call a nil value
"
);
