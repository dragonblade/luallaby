#[macro_export]
macro_rules! test {
    ($name:ident, $input:literal, $expected:literal) => {
        #[test]
        fn $name() {
            let mut vm = luallaby::VM::with_buffer();
            vm.run_str($input.as_bytes(), None).unwrap();

            let out = vm.take_buffer().unwrap();
            let out = String::from_utf8(out).unwrap();
            pretty_assertions::assert_str_eq!(out.trim(), $expected.trim());
        }
    };
}

#[macro_export]
macro_rules! test_regex {
    ($name:ident, $input:literal, $expected:literal) => {
        #[test]
        fn $name() {
            let mut vm = luallaby::VM::with_buffer();
            vm.run_str($input.as_bytes(), None).unwrap();

            let out = vm.take_buffer().unwrap();
            let out = String::from_utf8(out).unwrap();
            let re = regex::Regex::new($expected.trim()).unwrap();
            assert!(re.is_match(out.trim()));
        }
    };
}

#[macro_export]
macro_rules! test_err {
    ($name:ident, $input:literal, $err:pat) => {
        #[test]
        fn $name() {
            let mut vm = luallaby::VM::with_buffer();
            let res = vm.run_str($input.as_bytes(), None);

            match res {
                Ok(..) => assert!(false, "expected error"),
                Err(e) => match e {
                    luallaby::Error::Lua { typ, .. } => assert!(matches!(typ, $err)),
                    _ => assert!(false, "expected parse error"),
                },
            }
        }
    };
}
