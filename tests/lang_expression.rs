mod macros;

use luallaby::LuaError;

test!(
    expressions_basic,
    "
print(1 + 2)
print(1 - 2)
print(1 + 2 - 3)
print(1 + 2 * 2)
print((1 + 2) * 2)
",
    "
3
-1
0
5
6
"
);

test!(
    expressions_precedence,
    "
",
    "
"
);

test!(
    ops_arithmetic,
    "
print(1 + 2)
print(1.1 + 2)
print(\"1.1\" + \"2\")
print(1 - 2)
print(1.1 - 2)
print(\"1.1\" - \" 2 \")
print(2 * 3)
print(math.type(3 * 2.0))
print(3 * 2.2)
print(\"3\" * \"2.2 \")
print(1 / 2)
print(1 / 7)
print(1 / 9)
print(\"  1.0e0  \" / \"9 \")
print(1 // 2)
print(5 // 1.4)
print(\"5\" // \" 1.4\")
print(5 % 3)
print(5 % \"  0x1.8p1\")
print(2 ^ 5)
print(\"2 \" ^ \"5 \")
",
    "
3
3.1
3.1
-1
-0.8999999999999999
-0.8999999999999999
6
float
6.6000000000000005
6.6000000000000005
0.5
0.14285714285714285
0.1111111111111111
0.1111111111111111
0
3.0
3.0
2
2.0
32.0
32.0
"
);

test!(
    ops_int_overflow,
    "
print((1 << 63) - 1)
print(9223372036854775807 + 1)
print(9223372036854775807 * 4)
",
    "
9223372036854775807
-9223372036854775808
-4
"
);

test_err!(
    ops_fldiv_divide_zero,
    "print(1 // 0)",
    LuaError::DivideByZero
);

test!(
    ops_bitwise,
    "
print(10 & 3)
print(10 | 7)
print(10 ~ 7)
print(4 << 2)
print(4 >> 2)
",
    "
2
15
13
16
1
"
);

test!(
    ops_shift_overflow,
    "
print(1 << 64)
print(1 >> 64)
print(-8 >> 1)
print(-8 << -1)
print(-8 >> -1)
print(1 << 63)
",
    "
0
0
9223372036854775804
9223372036854775804
-16
-9223372036854775808
"
);

test!(
    ops_concat,
    "
print(\"abc\" .. \"def\")
print(\"abc\" .. 456)
print(123 .. \"def\")
print(123 .. 456)
",
    "
abcdef
abc456
123def
123456
"
);

test_err!(
    ops_concat_invalid_type,
    "print(123 .. false)",
    LuaError::OperatorConcat(..)
);

test!(
    ops_equal,
    "
print(\"a\" == \"a\")
print(4 ~= \"a\")
print(4 == \"a\")
print(nil == nil)
print(true == false)
print(1 == 1)
print(3 == 4)
print(3.0 == 3)
print(3.0001 == 3)
",
    "
true
true
false
true
false
true
false
true
false
"
);

test!(
    ops_compare,
    "
print(4 < 3)
print(4 > 3)
print(3 >= 3)
print(\"a\" < \"b\")
print(4 < 4.6)
print(4 >= 4.6)
print(4 <= 4.0)
",
    "
false
true
true
true
true
false
true
"
);

test_err!(
    ops_compare_same_type,
    "print(4 < \"a\")",
    LuaError::OperatorCompare(..)
);

test!(
    ops_logical,
    "
print(10 or 20)
print(10 or error())
print(nil or \"a\")
print(nil and 10)
print(false and error())
print(false and nil)
print(false or nil)
print(10 and 20)
",
    "
10
10
a
nil
false
false
nil
20
"
);

test!(
    ops_unary,
    "
print(-5)
print(- -5)
print(- \" 5 \")
print(-(5))
print(~16)
print(not false)
print(not \"\")
print(#{1, 2, 3})
print(#{[1] = 1, [3] = 3})
",
    "
-5
5
-5
-5
-17
true
false
3
1
"
);

test_err!(
    coerce_float_to_int,
    "print(1 & 2.5)",
    LuaError::FloatToInt(..)
);

test_err!(
    coerce_non_arithmetic_1,
    "print(\"2\" & \"1\")",
    LuaError::OperatorBitwise(..)
);
test_err!(
    coerce_non_arithmetic_2,
    "print(\"2\" | \"1\")",
    LuaError::OperatorBitwise(..)
);
test_err!(
    coerce_non_arithmetic_3,
    "print(\"2\" ~ \"1\")",
    LuaError::OperatorBitwise(..)
);
test_err!(
    coerce_non_arithmetic_4,
    "print(\"2\" >> \"1\")",
    LuaError::OperatorBitwise(..)
);
test_err!(
    coerce_non_arithmetic_5,
    "print(\"2\" << \"1\")",
    LuaError::OperatorBitwise(..)
);
test_err!(
    coerce_non_arithmetic_6,
    "print(~ \"1\")",
    LuaError::OperatorBitwise(..)
);

// TODO: Precedence tests
