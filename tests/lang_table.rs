#[macro_use]
mod macros;

test!(
    table_list,
    "
t = { 3, 4, 5, 6 }
print(t[1], t[2], t[3], t[4])
",
    "
3\t4\t5\t6
"
);

test!(
    table_separator,
    "
t = { 3; 4, 5 }
print(t[1], t[2], t[3])
",
    "
3\t4\t5
"
);

test!(
    table_associative,
    "
t = { [false] = 3, [3] = 4, [\"f\"] = 5, g = 6 }
print(t[false], t[3], t.f, t[\"f\"], t.g)
",
    "
3\t4\t5\t5\t6
"
);

test!(
    table_mixed,
    "
t = { 3, [false] = 4, 5, f = 6 }
print(t[1], t[false], t[2], t.f)
",
    "
3\t4\t5\t6
"
);

test!(
    table_assign,
    "
t = { 3, [false] = 4, 5, f = 6 }
print(t[1], t[false], t[2], t.f)
t.f = 5
t[false] = \"a\"
print(t[1], t[false], t[2], t.f)
",
    "
3\t4\t5\t6
3\ta\t5\t5
"
);

test!(
    table_ref,
    "
a = {}
b = a
a.t = 1
print(a.t, b.t)
",
    "
1\t1
"
);

test!(
    table_float_as_int,
    "
a = {[1] = 4, [2.0] = 5, [3.3] = 6}
print(a[1.0], a[1], a[2], a[2.0], a[3], a[3.0], a[3.3])
",
    "
4\t4\t5\t5\tnil\tnil\t6
"
);

test!(
    table_trailing_separator,
    "
a = { 4, 5 ; }
b = { 8; 9 , }
print(a[1], a[2], b[1], b[2])
",
    "
4\t5\t8\t9
"
);

test!(
    table_field_lone_ident,
    "
u, v = 4, 9
a = { v, u }
print(a[1], a[2])
a = { v, u, }
print(a[1], a[2])
",
    "
9\t4
9\t4
"
);

test!(
    table_field_lone_exp,
    "
a = { math.abs(-1), math.abs(-2) ; }
print(a[1], a[2])
a = { math.abs(-1), math.abs(-2) }
print(a[1], a[2])
",
    "
1\t2
1\t2
"
);

test!(
    table_last_element,
    "
function f() return 1, 2 end
a = { f(), }
print(a[1], a[2])
a = { f(), 7 }
print(a[1], a[2])

function f(...)
    a = { ..., }
    print(a[1], a[2])
    a = { ..., 7 }
    print(a[1], a[2])
end
f(1, 2)
",
    "
1\t2
1\t7
1\t2
1\t7
"
);

// TODO: Nil and NaN not allowed as table keys
