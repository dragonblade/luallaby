use luallaby::LuaError;

mod macros;

test!(
    control_if,
    "
local a = true
if a then
    print(1)
end

local a = true
if not a then
    print(2)
end
",
    "
1
"
);

test!(
    control_if_else,
    "
local a = true
if a then
    print(1)
else
    print(0)
end

local a = false
if a then
    print(1)
else
    print(0)
end
",
    "
1
0
"
);

test!(
    control_if_elseif,
    "
local a = 4
if a == 4 then
    print(1)
elseif a == 3 then
    print(0)
end

local a = 3
if a == 4 then
    print(1)
elseif a == 3 then
    print(0)
end

local a = 2
if a == 4 then
    print(1)
elseif a == 3 then
    print(0)
end
",
    "
1
0
"
);

test!(
    control_if_elseif_else,
    "
local a = 9
if a > 7 then
    print(\"a>7:\"..a)
elseif a > 5 then
    print(\"7>a>5:\"..a)
elseif a > 3 then
    print(\"5>a>3:\"..a)
else
    print(\"3>a:\"..a)
end

local a = 6
if a > 7 then
    print(\"a>7:\"..a)
elseif a > 5 then
    print(\"7>a>5:\"..a)
elseif a > 3 then
    print(\"5>a>3:\"..a)
else
    print(\"3>a:\"..a)
end

local a = 4
if a > 7 then
    print(\"a>7:\"..a)
elseif a > 5 then
    print(\"7>a>5:\"..a)
elseif a > 3 then
    print(\"5>a>3:\"..a)
else
    print(\"3>a:\"..a)
end

local a = 2
if a > 7 then
    print(\"a>7:\"..a)
elseif a > 5 then
    print(\"7>a>5:\"..a)
elseif a > 3 then
    print(\"5>a>3:\"..a)
else
    print(\"3>a:\"..a)
end
",
    "
a>7:9
7>a>5:6
5>a>3:4
3>a:2
"
);

test!(
    control_while,
    "
local a = 0
while a < 5 do
    a = a + 1
    print(a)
end

local a = 0
while a ~= 0 do
    a = a + 1
    print(a)
end
",
    "
1
2
3
4
5
"
);

test!(
    control_while_break,
    "
local a = 0
while true do
    a = a + 1
    if a > 5 then
        break
        print(a)
    end
    print(a)
end
",
    "
1
2
3
4
5
"
);

test!(
    control_repeat_until,
    "
local a = 0
repeat
    local b = a + 1
    a = b
    print(b)
until b > 4
",
    "
1
2
3
4
5
"
);

test!(
    control_repeat_until_break,
    "
local a = 0
repeat
    a = a + 1
    if a == 3 then
        break
        print(a)
    end
    print(a)
until false
",
    "
1
2
"
);

test!(
    goto,
    "
goto label
print(false)
::label::
print(true)
",
    "
true
"
);

test!(
    goto_backwards,
    "
goto l2
::l1::
print(1)
goto l3
::l2::
print(2)
goto l1
::l3::
print(3)
",
    "
2
1
3
"
);

test_err!(
    goto_nested,
    "
goto label
print(false)
do
    ::label::
    print(true)
end
",
    LuaError::GotoLabelNotFound(..)
);

test!(
    goto_parent_scope,
    "
local a = 0
while true do
    print(true)
    a = a + 1
    if a > 3 then
        goto label
    end
end
::label::
",
    "
true
true
true
true
"
);

test!(
    control_for_num,
    "
for a = 0, 4 do
    print(a)
end
",
    "
0
1
2
3
4
"
);

test!(
    control_for_num_step,
    "
for a = 0, 4, 2 do
    print(a)
end

for a = 0, 4, 3 do
    print(a)
end
",
    "
0
2
4
0
3
"
);

test!(
    control_for_num_float,
    "
for a = 0, 4, 1.8 do
    print(a)
end

for a = 1.1, 4 do
    print(a)
end

for a = 0, 3.6 do
    print(a)
end
",
    "
0.0
1.8
3.6
1.1
2.1
3.1
0
1
2
3
"
);

test!(
    control_for_num_nested,
    "
for i=1, 3 do
    print('outer', i)
    for i=i, 3 do
        print('inner', i)
    end
end
",
    "
outer\t1
inner\t1
inner\t2
inner\t3
outer\t2
inner\t2
inner\t3
outer\t3
inner\t3
"
);

test!(
    control_for_num_negative,
    "
for i=4, 1, -1 do
    print(i)
end
",
    "
4
3
2
1
"
);

test!(
    control_for_gen,
    "
for i, v in ipairs({7, 3, 8, 4, 9}) do
    print(i, v)
end
",
    "
1\t7
2\t3
3\t8
4\t4
5\t9
"
);

test!(
    control_for_gen_list,
    "
function f(s, c)
    assert(s == nil)
    if c == nil then
        return \"Hello\"
    elseif c == \"Hello\" then
        return \"world!\"
    else
        return nil
    end
end

for i in f, nil, nil do
    print(i)
end
",
    "
Hello
world!
"
);

test!(
    control_for_gen_overload,
    "
b = {1, 2, 3}
for a, b in pairs(b) do
    print(a, b)
end
",
    "
3\t3
2\t2
1\t1
"
);
