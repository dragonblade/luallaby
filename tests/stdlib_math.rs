mod macros;

test!(
    math_abs,
    "
print(math.abs(-1))
print(math.abs(1))
print(math.abs(1.1))
print(math.abs(-1.1))
print(math.abs(math.mininteger)) -- will overflow and wrap around
",
    "
1
1
1.1
1.1
-9223372036854775808
"
);

test!(
    math_acos,
    "
print(math.acos(math.pi))
print(math.acos(0))
print(math.acos(1))
",
    "
NaN
1.5707963267948966
0.0
"
);

test!(
    math_asin,
    "
print(math.asin(math.pi))
print(math.asin(0))
print(math.asin(1))
",
    "
NaN
0.0
1.5707963267948966
"
);

test!(
    math_atan,
    "
print(math.atan(math.pi))
print(math.atan(0))
print(math.atan(2.4, 3.6))
",
    "
1.2626272556789118
0.0
0.5880026035475675
"
);

test!(
    math_ceil,
    "
print(math.ceil(-1))
print(math.ceil(1))
print(math.ceil(1.1))
print(math.ceil(-1.1))
print(math.ceil(-3e23))
",
    "
-1
1
2
-1
-3e23
"
);

test!(
    math_cos,
    "
print(math.cos(math.pi))
print(math.cos(0))
print(math.cos(1))
",
    "
-1.0
1.0
0.5403023058681398
"
);

test!(
    math_deg,
    "
print(math.deg(math.pi))
print(math.deg(0))
print(math.deg(2 * math.pi))
",
    "
180.0
0.0
360.0
"
);

test!(
    math_exp,
    "
print(math.exp(0))
print(math.exp(1))
",
    "
1.0
2.718281828459045
"
);

test!(
    math_floor,
    "
print(math.floor(-1))
print(math.floor(1))
print(math.floor(1.1))
print(math.floor(-1.1))
print(math.floor(-3e23))
",
    "
-1
1
1
-2
-3e23
"
);

test!(
    math_fmod,
    "
print(math.fmod(-1.2, 1))
print(math.fmod(2, 1))
print(math.fmod(2, 1.3))
print(math.fmod(-3, 1))
",
    "
0.8
0
0.7
0
"
);

test!(math_huge, "print(math.huge)", "inf");

test!(
    math_log,
    "
print(math.log(2.718281828459045))
print(math.log(4, 2))
print(math.log(100, 10))
",
    "
1.0
2.0
2.0
"
);

test!(math_max, "print(math.max(3, 4, -1, 4.2))", "4.2");

test!(
    math_maxinteger,
    "print(math.maxinteger)",
    "9223372036854775807"
);

test!(math_min, "print(math.min(3, 4, -1, 4.2))", "-1");

test!(
    math_mininteger,
    "print(math.mininteger)",
    "-9223372036854775808"
);

test!(
    math_modf,
    "
print(math.modf(4.79))
print(math.modf(-3e23))
",
    "
4\t0.79
-3e23\t0.0
"
);

test!(math_pi, "print(math.pi)", "3.141592653589793");

test!(
    math_rad,
    "
print(math.rad(0))
print(math.rad(180))
print(math.rad(360))
",
    "
0.0
3.141592653589793
6.283185307179586
"
);

test!(
    math_random,
    "
print(math.randomseed(1, 2))
print(math.random(0))
print(math.random())
",
    "
1\t2
8291693048688576641
0.22576880156399304
"
);

test!(
    math_sin,
    "
print(math.sin(0))
print(math.sin(1))
print(math.sin(math.pi/2))
",
    "
0.0
0.8414709848078965
1.0
"
);

test!(
    math_sqrt,
    "
print(math.sqrt(-1))
print(math.sqrt(1))
print(math.sqrt(2))
print(math.sqrt(4))
",
    "
NaN
1.0
1.4142135623730951
2.0
"
);

test!(
    math_tan,
    "
print(math.tan(0))
print(math.tan(1))
print(math.tan(math.pi/2))
",
    "
0.0
1.5574077246549023
1.633123935319537e16
"
);

test!(
    math_tointeger,
    "
print(math.tointeger(3.0000000000000001))
print(math.tointeger('3.0'))
print(math.tointeger(3.1))
",
    "
3
3
nil
"
);

test!(
    math_type,
    "
print(math.type(0))
print(math.type(1.0))
print(math.type(math.pi/2))
print(math.type({}))
print(math.type(0x13121110090807060504030201))
",
    "
integer
float
float
nil
integer
"
);

test!(
    math_ult,
    "
print(math.ult(3, 4))
print(math.ult(4, 3))
print(math.ult(-3, 4))
",
    "
true
false
false
"
);
