use luallaby::LuaError;

mod macros;

test!(
    hello_world,
    "
print(\"Hello world!\")
print('Hello world!')
",
    "
Hello world!
Hello world!
"
);

test!(
    assignment,
    "
a = \"Hello world!\"
b = a
print(b)
",
    "
Hello world!
"
);

test!(
    assignment_multiple,
    "
x, y = 1, 2
print(x, y)
x, y = y, x
print(x, y)
",
    "
1\t2
2\t1
"
);

test!(
    assignment_multiple_table,
    "
a = {}
i = 3
i, a[i] = i+1, 20
print(i, a[3], a[4])

a = {k = 1}
b = a
a, a[1] = {}, 2
print(a[1], a.k, b[1], b.k)
",
    "
4\t20\tnil
nil\tnil\t2\t1
"
);

test!(
    undefined_var,
    "
print(a)
",
    "
nil
"
);

test_regex!(
    value_to_string,
    "
a = nil
b = true
c = 0.1
d = \"a\"
e = {}
function f()
end
print(a)
print(b)
print(c)
print(d)
print(e)
print(f)
",
    r"
nil
true
0.1
a
table: 0x([a-fA-F0-9])+
function: 0x([a-fA-F0-9])+
"
);

test_err!(
    local_const,
    "
local a <const> = 5
a = 6
",
    LuaError::AssignToConst(..)
);

test!(
    local_scopes,
    "
local a = 5
do
    local a = 7
    local b = 6
    print(a, b)
end
print(a, b)
",
    "
7\t6
5\tnil
"
);

test!(
    local_redef,
    "
local a = 5
function f()
    print(a)
end
local a = 6
f()

local b = 7
function g()
    print(b)
end
b = 8
g()
",
    "
5
8
"
);

test!(
    lexer_numbers,
    "
print(0)
print(0.1)
print(.1)
print(2e-2)
print(2E2)
print(2e+2)
print(0xfp2)
print(0x1.4p3)
",
    "
0
0.1
0.1
0.02
200.0
200.0
60.0
10.0
"
);

test!(
    lexer_comments,
    "
do
  -- one comment
  -- two comments
  print(true)
  -- three comments
end
",
    "
true
"
);

test!(
    lexer_long_comments,
    "
-- print(2)
print(1)
--[=[print(2)
print(1)
]=]
print(3)
",
    "
1
3
"
);

test!(
    lexer_empty_comment,
    "--
print(1)
",
    "
1
"
);

test_err!(assert_fail, "assert(false)", LuaError::AssertFailed);

test!(assert_pass, "assert(0)", "");

// TODO: Variable assignment (+local), value matching/overflow

test!(
    long_string,
    "
print([[s]]=='s')
print([[
s]]=='s')
print([[

s]]=='\\ns')
",
    "
true
true
true
"
);
