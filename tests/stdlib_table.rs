mod macros;

test!(
    table_concat,
    "
t = { 1, 2, 3 }
print(table.concat(t))
print(table.concat(t, ','))
print(table.concat(t, 4, 2))
print(table.concat(t, 7, 2, 2))
",
    "
123
1,2,3
243
2
"
);

test!(
    table_insert,
    "
t = { 1, 2, 3 }
table.insert(t, 4)
print(t[3], t[4], t[5])
table.insert(t, 3, true)
print(t[2], t[3], t[4], t[5], t[6])
",
    "
3\t4\tnil
2\ttrue\t3\t4\tnil
"
);

test!(
    table_move,
    "
t = { 1, 2, 3 }
table.move(t, 1, 3, 2)
print(t[0], t[1], t[2], t[3], t[4], t[5])

t = { 1, 2, 3 }
table.move(t, 2, 3, 1)
print(t[0], t[1], t[2], t[3], t[4], t[5])

a = { 1, 2, 3 }
b = {}
table.move(a, 1, 3, 4, b)
print(a[1], a[2], a[3])
print(b[4], b[5], b[6])
",
    "
nil\t1\t1\t2\t3\tnil
nil\t2\t3\t3\tnil\tnil
1\t2\t3
1\t2\t3
"
);

test!(
    table_pack,
    "
t = table.pack(1, 2, 3)
print(t[1], t[2], t[3], t.n)
t = table.pack()
print(t[1], t.n)
t = table.pack(1, nil, 3)
print(t[1], t[2], t[3], t.n, #t)
",
    "
1\t2\t3\t3
nil\t0
1\tnil\t3\t3\t1
"
);

test!(
    table_remove,
    "
t = { 1, 2, 3, 4, 5 }
print(table.remove(t, 3))
print(t[1], t[2], t[3], t[4], t[5])
print(table.remove(t))
print(t[1], t[2], t[3], t[4], t[5])
t = { n = 4, t = 5 }
print(table.remove(t, \"n\"))
print(t.n, t.t)
",
    "
3
1\t2\t4\t5\tnil
5
1\t2\t4\tnil\tnil
4
nil\t5
"
);

test!(
    table_sort,
    "
t = { 4, 5, 1, 3, 2 }
table.sort(t)
print(t[1], t[2], t[3], t[4], t[5])
table.sort(t, function (a, b) return a > b end)
print(t[1], t[2], t[3], t[4], t[5])
",
    "
1\t2\t3\t4\t5
5\t4\t3\t2\t1
"
);

test!(
    table_unpack,
    "
t = { 1, 2, 3, 4, n = 5 }
print(table.unpack(t))
print(table.unpack(t, 2))
print(table.unpack(t, 2, 3))
print(table.unpack(t, 3, 1))
",
    "
1\t2\t3\t4
2\t3\t4
2\t3
"
);
